package no.akerbaek.epistula;

import android.content.SharedPreferences;
import android.util.Log;

import androidx.core.text.HtmlCompat;
import androidx.preference.PreferenceManager;

import com.sun.mail.util.MailConnectException;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Properties;
import java.util.TimeZone;
import java.util.Vector;

import javax.activation.DataHandler;
import javax.mail.Address;
import javax.mail.AuthenticationFailedException;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.search.HeaderTerm;

import static javax.mail.internet.MimeUtility.decodeText;

interface Mailrequest
{
    Runnable getRunnable();
    void init();
    void doIt();
    void postIt(int what) throws InstantiationException, IllegalAccessException;

    void setThread(Thread t);
    void setRunning();
    Boolean isRunning();
}

class EpMailbase implements Mailrequest
{
    public EpRunnable epRunnable;

    Properties properties = null;
    protected Session session = null;
    protected Store store = null;
    protected Folder inbox = null;

    Vector<String> mAtts  = new Vector<>();
    Vector<EpAttachment> mAttach = new Vector<>();

    String mDownload = null;
    int mPosition;

    protected MainActivity main;
    public Node node;

    private Boolean fin = false;
    private Boolean run = false;

    public EpMailbase(MainActivity m, Node n)
    {
        main = m;
        node = n;
        // Create the runnables
        epRunnable = new EpRunnable(this);
    }

    public void init()
    {
        fin = false;
        run = false;
    }

    public static String getOneHeader(Message m, String id) throws MessagingException
    {
        String [] h = m.getHeader(id);
        if(h != null && h.length > 0) return h[0]; else return "";
    }

    public static String getParentId(Message m) throws MessagingException
    {
        String mpar = getOneHeader(m, "In-Reply-To");
        // Log.i("ReadMailList","In-Reply-To: " + mpar);
        if(!mpar.trim().isEmpty())
        {
            return mpar.trim();
        }
        mpar = getOneHeader(m, "References");
        String delims = "[ ]+";
        String[] tokens = mpar.trim().split(delims);
        mpar = tokens[tokens.length-1].trim();
        if(!mpar.isEmpty())
            return mpar;
        //Log.i("ReadMailList","Last Reference: " + tokens[tokens.length-1]);
        return "";
    }

    public static String fromName(Message m)throws MessagingException, UnsupportedEncodingException
    {
        Address[] a = m.getFrom();
        if(a==null || a.length<1)
            return "?";
        return decodeText(String.valueOf(a[0]));
    }

    public void inMail(Node folderNode, Node holderNode, Message message) throws MessagingException, UnsupportedEncodingException
    {
        Node contacts = EpManager.getContacts();
        // String fn = (folderNode.getFolderName() != null) ? folderNode.getFolderName() : "INBOX";
        String mid  = EpMailbase.getOneHeader(message, "Message-ID");
        String mpar = EpMailbase.getParentId(message);
        Node mn;
        if(mpar.isEmpty() || mpar.contentEquals(mid))
            mn = folderNode;
        else
            mn = EpManager.getNode(mpar);
        if(mn==null)
        {
            // Create mother node with mpar as id
            // Temporarily place mother in folder
            mn = EpManager.createNode(folderNode, mpar,MailFragment.class,false);
            mn.setTitle(message.getSubject());
            mn.setFolderName("unknown");
        }
        else if(folderNode.isMotherOf(mn))
            mn.addMother(folderNode);  // Fix potentially broken link
        // Log.i("ReadMailList","Parent: " + mpar);

        Node nxt = EpManager.getNode(mid);
        // If email already created
        if(nxt != null)
        {
            // Copy to folder
//                if(mn.getType()==BoxFragment.class)
            Node mnb = nxt.getMother(); //   find its old parent
            // If nxt is dummy moveto mn else
            if(nxt.isPermanent() || (mnb!=null && mnb.isFilter()))   // Do not want to delete from unread special folder
                nxt.addMother(mn); // Keep previous mother
            else
                nxt.transferTo(mnb,mn);
//                nxt.moveTo(mn);  // Forget mother
        }
        else
            nxt = EpManager.createNode(mn, mid ,MailFragment.class, true);
        if(holderNode != null)  //Special treatment unread special folder
            nxt.addMother(holderNode);
        // if the mother is not read yet
        // if(mn.getNumber()==0)
        // nxt.addBabysitter(folderNode);  // answer mail is accessible from folder
        nxt.setFolderName( message.getFolder().getFullName());
        nxt.setNumber(message.getMessageNumber());
        nxt.setMotherId(mpar);
        nxt.setTitle(message.getSubject());

        Calendar cal = Calendar.getInstance(TimeZone.getDefault());
        cal.setTime(message.getReceivedDate());
        nxt.setCalendar(cal);

        String fromNam = fromName(message);
        Node user = EpManager.getNode(fromNam);
        if(user==null)
            user = EpManager.createNode(contacts, fromNam, BoxFragment.class, true);
        user.setFolderName("all");
        user.setUser(true);

        nxt.setFrom(fromNam);

        nxt.setUnread(!message.isSet(Flags.Flag.SEEN));

    }

    public void inMail(Node folderNode, Message message) throws MessagingException, UnsupportedEncodingException
    {
        inMail(folderNode, null, message);
    }


    public void  inChildren(Node node, String mpar) throws MessagingException, UnsupportedEncodingException, InterruptedException
    {

        Node cats = EpManager.getFolders();
        if(cats != null) for(int i=0;i<cats.getSize();i++)
        {
            Node f = cats.getChild(i);
            String fn = f.getName();
            if(fn.contentEquals("Trash")) continue;  // Skip trash folder

            if(!store.isConnected()) store.connect(); // Not good enough. Needs time to connect. Async?
            Folder inbox = store.getFolder(fn);
            if(!inbox.exists()) continue;
            while(inbox.isOpen()) this.wait(1000);
            inbox.open(Folder.READ_ONLY);
            if(!inbox.isOpen())
            {
                EpManager.alert(EpManager.getContext().getString(R.string.noopen) + " " + fn);
                continue;
            }
            Message[] messages = inbox.search(new HeaderTerm("In-Reply-To", mpar));
            for (Message message : messages)
            {
                String cMid = getOneHeader(message, "Message-ID");
                Node cNode = EpManager.getNode(cMid);
                if (cNode != null)
                {
//                    cNode.moveTo(node);
                    cNode.addMother(node);
                }
                else
                    cNode = EpManager.createNode(node, cMid, MailFragment.class, true);
                cNode.setFolderName(fn);
                cNode.setNumber(message.getMessageNumber());
                cNode.setMotherId(mpar);
                cNode.setTitle(message.getSubject());
                cNode.setFrom(fromName(message));
            }
            if(inbox.isOpen()) inbox.close();
        }

    }

    /**
     * Retrieve text content and attachments of the message.
     */
    protected String parseContent(Part p) throws
            MessagingException, IOException
    {
        String parsedHtml =null;
        StringBuilder parsedText;
        if (p.isMimeType("text/*"))
        {
            if(p.isMimeType("text/html"))
            {
                String s = (String)p.getContent();
                int bodyIndex = s.indexOf("<body");
                if(bodyIndex == -1) bodyIndex = 0;
                String r = s.substring(bodyIndex);
                return HtmlCompat.fromHtml(r, HtmlCompat.FROM_HTML_MODE_LEGACY).toString();
            }
            else if(p.isMimeType("text/plain"))
            {
                String s = (String)p.getContent();
                return s;
            }
            else
                return "\nUnhandled mimetype " + p.getContentType() + "\n\n";
        }
        else if (p.isMimeType("multipart/alternative"))
        {
            // prefer plain text over html text
            Multipart mp = (Multipart)p.getContent();
            // String text = null;
            for (int i = 0; i < mp.getCount(); i++)
            {
                Part bp = mp.getBodyPart(i);
                if(bp.isMimeType("text/plain"))
                    return parseContent(bp);
                else
                    parsedHtml = parseContent(bp);
            }
            return parsedHtml;

        }
        else if (p.isMimeType("multipart/*"))
        {
            Multipart mp = (Multipart)p.getContent();
            parsedText = new StringBuilder();
            for (int i = 0; i < mp.getCount(); i++)
            {
                parsedText.append(parseContent(mp.getBodyPart(i)));
            }
            return parsedText.toString();
        }
        else
        {
            String name = p.getFileName();
            if(name != null)
            {
                if(!mAtts.contains(name))
                {
                    mAtts.add(name);
                    //                  addType(p.getContentType());
                    //                  mPaths.add(null);

                    EpAttachment att = new EpAttachment(name,null,p.getContentType(), 0);
                    // att.setPart((BodyPart)p);
                    mAttach.add(att);
                }


            }

            if(name != null && name.equals(mDownload)  &&  mAttach.size() > mPosition  && mAttach.get(mPosition).getPath() != null)
            {
                // MimeBodyPart m = (MimeBodyPart)p;
                //File outputDir = main.getCacheDir();
                // Decide overwrite old path?

                // File path = mPaths.get(mPosition);
                OutputStream path = (OutputStream) EpManager.getContext().getContentResolver().openOutputStream(mAttach.get(mPosition).getPath(),"w");

                // File outputFile = new File(dir,name);
                Log.i("parseContent", "Attachment " + name + " saving in " + mAttach.get(mPosition).getPath().toString());
                //m.saveFile(path);

                DataHandler n = p.getDataHandler();
                n.writeTo(path);
                path.flush();
                path.close();
                EpManager.handleState(this, 8);
            }
            // If forwarding attachments

            else if(name != null && mDownload == null && mPosition == -1)
            {
                EpAttachment a = mAttach.get(mAtts.indexOf(name));
                if(a.isMode(4))
                {
                    Log.i("parseContent", "Forward attachment " + name);
                    a.setPart((BodyPart)p);
                }
            }

            Log.i("parseContent", "Attachment " + name);
            return "";
        }
    }

    public void catchME (MessagingException e)
    {

        if(e.getClass() == NoSuchProviderException.class)
        {
            EpManager.alert(EpManager.getContext().getString(R.string.noconnect));
        }
        else if(e.getClass() == MailConnectException.class)
        {
            EpManager.alert(EpManager.getContext().getString(R.string.noconnect));
        }
        else if(e.getClass() == AuthenticationFailedException.class)
        {
            EpManager.alert(EpManager.getContext().getString(R.string.nouser));
        }
        else
        {
            EpManager.alert(e.toString());
            e.printStackTrace();
        }
        EpManager.handleState(this, 0);
    }
/*
    String[] mailList;

    public void setMailList(String[] ml,int len)
    {
        mailList = new String[len];

        System.arraycopy(ml, 0, mailList, 0,len);
    }

    public String[] getMailList()
    {
        return mailList;
    }
*/
    public void setFinished()
    {
        fin=true;
        run=false;
    }

    public Boolean isFinised()
    {
        return fin;
    }

    public void setRunning()
    {
        run=true;
    }

    public Boolean isRunning()
    {
        return run;
    }

    public void doIt()
    {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(main); // main.getPreferences(Context.MODE_PRIVATE);


        properties = new Properties();
        properties.setProperty("mail.host", prefs.getString("imap_host","mail.domeneshop.no"));
        String port = prefs.getString("imap_port","143");  // 143 / 993 ?
        if(port !=null &&  !port.isEmpty())
            properties.setProperty("mail.port", port);
        properties.setProperty("mail.transport.protocol", "imaps");

        final String userName = prefs.getString("imap_user",null);// provide user name
        final String password = prefs.getString("imap_pass",null);// provide password

        session = Session.getInstance(properties,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(userName, password);
                    }
                });

    }

    public void postIt(int what) throws InstantiationException, IllegalAccessException

    {

    }


    public void setThread(Thread t)
    {

    }

    // Returns the instance that downloaded the thing
    public Runnable getRunnable()
    {
        return epRunnable;
    }
/*
    public void onItemClick(AdapterView<?> l, View v, int position, long id)
    {
    }
*/


}

