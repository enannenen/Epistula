package no.akerbaek.epistula;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;


import androidx.fragment.app.Fragment;

import static android.view.ActionMode.TYPE_FLOATING;


public class EpTextView extends androidx.appcompat.widget.AppCompatTextView
{
    private ActionMode mActionMode;
    public ActionMode.Callback mActionModeCallback;

    @Override
    protected void onCreateContextMenu(ContextMenu menu) {
        super.onCreateContextMenu(menu);
        //if(mActionModeCallback == null) mActionModeCallback = new CustomActionModeCallback();
        //setCustomSelectionActionModeCallback(mActionModeCallback);
    }

    public EpTextView(Context context)
    {
        super(context);
    }

    public EpTextView(Context context, AttributeSet attrs)
    {
        super(context,attrs);
        if(mActionModeCallback == null) mActionModeCallback = new CustomActionModeCallback();
        setCustomSelectionActionModeCallback(mActionModeCallback);
     }

    public EpTextView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
    }

    class CustomActionModeCallback implements ActionMode.Callback
    {

            // Called when the action mode is created; startActionMode() was called
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu)
            {
                mActionMode = mode;
                // remove the items you don't want individually:
                if (android.os.Build.VERSION.SDK_INT >= 23)
                  menu.removeItem(android.R.id.shareText);
                menu.removeItem(android.R.id.selectAll);


                // Inflate a content_menu resource providing context content_menu items
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.content_menu, menu);

                return true;
            }

            // Called each time the action mode is shown.
            // Always called after onCreateActionMode, but
            // may be called multiple times if the mode is invalidated.
            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu)
            {
                //MenuInflater inflater = mode.getMenuInflater();
                //inflater.inflate(R.menu.content_menu, menu);
                //menu.add(R.string.cite);
                return false; // Return false if nothing is done
            }

            // Called when the user selects a contextual content_menu item
            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item)
            {
                try
                {
                    final int b1 = R.id.button1;
                    final int b2 = R.id.button2;

                    switch (item.getItemId())
                    {
                        case b1:
                        case b2:
                            int start = getSelectionStart();
                            int end = getSelectionEnd();
                            String s = getText().subSequence(start, end).toString();
                            Log.i("button ", s);

                            EpFragmentInterface frag = EpManager.getCurrentNode().getFragment();
                            if(frag instanceof EpFragment)
                                ((EpFragment)frag).editMail(start, end, item.getItemId());

                            break;

                        default:
                            // This essentially acts as a catch statement
                            // If none of the other cases are true, return false
                            // because the action was not handled
                            return false;
                    }
                }
                catch(Fragment.InstantiationException e)
                {
                    e.printStackTrace();
                }
                catch(IllegalAccessException e)
                {
                    e.printStackTrace();
                }
                mode.finish(); // An action was handled, so close the CAB
                return true;
            }

            // Called when the user exits the action mode
            @Override
            public void onDestroyActionMode(ActionMode mode)
            {
                //mActionMode = null;
            }

    }




    @Override
    public ActionMode.Callback getCustomSelectionActionModeCallback ()
    {
        if(mActionModeCallback == null)
            mActionModeCallback = new CustomActionModeCallback();
        return mActionModeCallback;
    }

    public ActionMode startActionMode(ActionMode.Callback callback)
    {

        if(mActionModeCallback == null)
            mActionModeCallback = new CustomActionModeCallback();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            mActionMode = super.startActionMode(mActionModeCallback, TYPE_FLOATING);
        } else
        {
            mActionMode = super.startActionMode(mActionModeCallback);
        }

        return mActionMode;
    }

    public void initActionMode()
    {
        /*
        ViewParent parent = getParent();
        if (parent == null)
        {
            return null;
        }
         */
        if(mActionModeCallback == null)
            mActionModeCallback = new CustomActionModeCallback();
        //return parent.startActionModeForChild(this, mActionModeCallback, TYPE_FLOATING);
        setCustomSelectionActionModeCallback(mActionModeCallback);
        EpManager.getContext().openContextMenu(this);
        if(mActionMode == null)
        {

/*
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            {
                mActionMode = super.startActionMode(mActionModeCallback, TYPE_FLOATING);
            } else
            {
                mActionMode = super.startActionMode(mActionModeCallback);
            }
*/
        }
        else
            mActionMode.invalidate();
    }


/*
    @Override
    public boolean onLongClick(View v)
    {
        if (mActionMode != null)
        {
            return false;
        }

        // mActionMode = startActionMode(mActionModeCallback);
        // mActionModeCallback = new CustomActionModeCallback();
        // mActionMode = v.startActionMode(mActionModeCallback);

        ViewParent parent = v.getParent();
        if (parent == null)
        {
            return false;
        }
        mActionMode =  parent.startActionModeForChild(v, mActionModeCallback);
        v.setSelected(true);
        return true;
/*
        mActionMode = startActionMode(mActionModeCallback);

        v.setSelected(true);
        return true;
     }
        */
}
