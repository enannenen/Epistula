package no.akerbaek.epistula;

import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.preference.EditTextPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

public class EpSettingsFragment extends PreferenceFragmentCompat implements EpFragmentInterface
{

    EpMailTest sample;
    View view;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey)
    {
        view = getView();
        setPreferencesFromResource(R.xml.preferences, rootKey);

        EditTextPreference preference = findPreference("imap_pass");

        if (preference != null)
        {
            preference.setOnBindEditTextListener(
                    new EditTextPreference.OnBindEditTextListener()
                    {
                        @Override
                        public void onBindEditText(@NonNull EditText editText)
                        {
                            editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        }
                    });
        }

        preference = findPreference("smtp_pass");

        if (preference!= null)
        {
            preference.setOnBindEditTextListener(
                    new EditTextPreference.OnBindEditTextListener()
                    {
                        @Override
                        public void onBindEditText(@NonNull EditText editText)
                        {
                            editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        }
                    });
        }

        Preference button = findPreference("imap_test");
        if(button != null)
        {
            button.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference)
                {
                    sample = new EpMailTest(EpManager.getContext(), null);
                    if(sample.isRunning())
                        ;
                    else
                        EpManager.startRequest(sample, EpManager.getContext());
                    return true;
                }
            });
        }

        button = findPreference("smtp_test");
        if(button != null)
        {
            button.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
            {
                @Override
                public boolean onPreferenceClick(Preference preference)
                {
                    sample = new EpMailTest(EpManager.getContext(),EpManager.getSettings());
                    if(sample.isRunning())
                        ;
                    else
                        EpManager.startRequest(sample, EpManager.getContext());return true;
                }
            });
        }

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        EpManager.setCurrentNode(EpManager.getSettings());
        EpManager.fixButtons();

    }


    public void editMail(int start, int stop, int type) {}
    public void reload(){}

    public Boolean paint(Node n) { return false; }
}
