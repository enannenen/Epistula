package no.akerbaek.epistula;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

interface EpFragmentInterface
{
    void reload();
    Boolean paint(Node n);
    void editMail(int start, int stop, int type);
}

public class EpFragment extends Fragment implements EpFragmentInterface
{
    protected static final String ARG_NAME = "name";
    protected static final String ARG_TEXT = "text";
    protected static final String ARG_START = "start";
    protected static final String ARG_END = "end";
    protected static final String ARG_TYPE = "type";
    Node node;
    View mView;


// TODO: save and recover instance

    public static EpFragment newInstance(String name)
    {
        EpFragment fragment = new EpFragment();
        Bundle args = new Bundle();
        args.putString(ARG_NAME, name);

        fragment.setArguments(args);

        return fragment;
    }

    public void setNode(Node n)
    {
        node = n;
    }

    @Override
    public void onSaveInstanceState(@NonNull final Bundle outState)
    {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (BuildConfig.DEBUG) { if (getArguments()==null) throw new AssertionError("no arguments"); }

        if (node!=null) return;

        if (savedInstanceState != null)
        {
            String name = savedInstanceState.getString(ARG_NAME);
            node = EpManager.getNode(name);
            if (node!=null) return;
        }
        if (getArguments() != null)
        {
            String name = getArguments().getString(ARG_NAME);
            node = EpManager.getNode(name);
            if (BuildConfig.DEBUG) { if (node==null) throw new AssertionError("node is null " + name); }
        }
     }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        mView = view;
        TextView boxName = (TextView) (view.findViewById(R.id.name));
        if (boxName != null) boxName.setText(node.getTitle());

        // Set current node and refresh activity buttons

        EpManager.setCurrentNode(node);
        Node cm=EpManager.getCurrentMother();
        if(cm == null || !cm.isMotherOf(node))  //if backstacking we need to use one arbitrary mother
            EpManager.setCurrentMother(node.getMother());
        EpManager.fixButtons();
    }

    public void editMail(int start, int stop, int type)
    {

    }

    public void reload(){}

    public Boolean paint(Node n) { return false; }

}

