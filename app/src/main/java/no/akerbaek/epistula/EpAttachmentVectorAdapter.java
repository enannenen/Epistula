package no.akerbaek.epistula;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.Vector;

import javax.mail.internet.MimeUtility;

public class EpAttachmentVectorAdapter extends ArrayAdapter<String>
{

    Vector<EpAttachment> mAttach;

    int mResource;
    EpMessageFragment mFrag;

    public EpAttachmentVectorAdapter(Context context, int resource, int textViewResourceId, Vector<String> names, Vector<EpAttachment> attach, EpMessageFragment frag)
    {
        super(context, resource, textViewResourceId, names);
        mAttach = attach;

        mResource = resource;
        mFrag = frag;
    }

    @Override
    public int getCount()
    {
        return mAttach.size();
    }

    @Override
    public String getItem(int p)
    {
        String ret;
        try
        {
            ret = MimeUtility.decodeText(mAttach.get(p).getName());
        }
        catch(UnsupportedEncodingException e)
        {
            ret = "xxx";
        }
        Log.i("getItem",ret);
        return ret;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        View v = convertView;

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(v==null)
            v = inflater.inflate(mResource, null);

        TextView textView = (TextView) v.findViewById(R.id.textView);
        Button saveBI = (Button) v.findViewById(R.id.saveIcon);
        Button openBI  = (Button) v.findViewById(R.id.open);
        Button remBI   = (Button) v.findViewById(R.id.remove);
        CheckBox resendC   = (CheckBox) v.findViewById(R.id.resend);

        String ret = getItem(position);
        if(ret.length() > 39)
            ret = ret.substring(0,18) + "…" + ret.substring(ret.length()-18,ret.length()-1);
        textView.setText(ret);
        //if(mNode.getChild(position).isUnread())
        textView.setTextColor(Color.rgb(200,0,0));
        //saveB.setText(EpManager.getContext().getString(R.string.save));
        //openB.setText(mVector.get(position));



        View.OnClickListener saveCL = new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.i("Attachment", "save button clicked");
                mFrag.mMailReader.setAttachmentDownload(position);
/*
                if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_MEDIA_LOCATION) != PackageManager.PERMISSION_GRANTED)
                {
                    Log.i("Attachment", "no permission A" );
                    // Directly ask for the permission.
                    mFrag.requestPermissions(
                            new String[] { Manifest.permission.ACCESS_MEDIA_LOCATION },
                            124);

                    //return;
                }

 */
                if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                {
                    Log.i("Attachment", "no permission W" );
                    mFrag.requestPermissions(
                            new String[] { Manifest.permission.WRITE_EXTERNAL_STORAGE },
                            125);
                    return;
                }
                Intent intent;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT)
                {
                    intent = new Intent()
                            .setType(mAttach.get(position).getType())
                            .putExtra(Intent.EXTRA_TITLE, getItem(position))
                            .setAction(Intent.ACTION_CREATE_DOCUMENT);


                    mFrag.startActivityForResult(Intent.createChooser(intent, "Select a file"), 123);
                }
                else
                {
                    Log.i("Attachment", "dest path unknown " );
                }
            }

        };



        View.OnClickListener openCL = new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.i("Attachment","open button clicked");
                Uri path;

                if(mAttach.size() <= position)
                {
                                             File dir  = mFrag.requireContext().getExternalCacheDir();
                                             File file = new File(dir,getItem(position));
                                             path = FileProvider.getUriForFile(mFrag.requireContext(),BuildConfig.APPLICATION_ID + ".provider",file);
                                             EpAttachment att = new EpAttachment(getItem(position),path,null, 0);
                                             mAttach.add(position,att);
                                             mFrag.mMailReader.setAttachmentDownload(position);
                                             mFrag.reload();
                }
                else if((path = mAttach.get(position).getPath())== null)
                {
                                             File dir  = mFrag.requireContext().getExternalCacheDir();
                                             File file = new File(dir,getItem(position));
                                             path = FileProvider.getUriForFile(mFrag.requireContext(),BuildConfig.APPLICATION_ID + ".provider",file);
                                             EpAttachment att = mAttach.get(position);
                                             att.setPath(path);
                                             mFrag.mMailReader.setAttachmentDownload(position);
                                             mFrag.reload();
                }
                else
                {
                                             String type = mAttach.get(position).getType();
                                             Log.i("Attachment","path " + path.toString());

                                             Intent openintent = new Intent(Intent.ACTION_VIEW);
                                             openintent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                             openintent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                             openintent.setDataAndType(path, type);
                                             try
                                             {
                                                 Log.i("Attachment","start activity " + path.toString() + " " + type);
                                                 mFrag.startActivity(openintent);
                                             }
                                             catch (ActivityNotFoundException e)
                                             {
                                                 e.printStackTrace();
                                             }
                }
            }
        };


        CompoundButton.OnCheckedChangeListener checkC = new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                Log.i("AttachmentVA","checkbox clicked");
                if(isChecked)
                {
                    mAttach.get(position).setMode(4);
                }
                else
                {
                    mAttach.get(position).clrMode(4);
                }

            }

        };

        View.OnClickListener removeCL = new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.i("Attachment", "remove button clicked " + position);

                EpAttachmentVectorAdapter.super.remove(getItem(position));
                mAttach.remove(position);
            }
        };

        textView.setOnClickListener(openCL);

//        if(!mAttach.get(position).isMode(0) )
        if(mFrag.getClass() == MailFragment.class )
        {
            saveBI.setVisibility(View.VISIBLE);
            openBI.setVisibility(View.VISIBLE);
            remBI.setVisibility(View.GONE);
            resendC.setVisibility(View.GONE);

            saveBI.setOnClickListener(saveCL);
            openBI.setOnClickListener(openCL);
        }
        else
        {
            saveBI.setVisibility(View.GONE);
            openBI.setVisibility(View.GONE);
            if(mAttach.get(position).isMode(2) )
            {
                remBI.setVisibility(View.VISIBLE);
                resendC.setVisibility(View.GONE);

                remBI.setOnClickListener(removeCL);
            }
            else
            {
                resendC.setVisibility(View.VISIBLE);
                remBI.setVisibility(View.GONE);

                resendC.setOnCheckedChangeListener(checkC);
            }
        }

        return v;

    }

}