package no.akerbaek.epistula;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import androidx.core.text.HtmlCompat;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Vector;

import javax.activation.DataHandler;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.search.HeaderTerm;

class EpMailRead extends EpMailbase implements Mailrequest
{

    public EpMailRead(MainActivity m, Node n)
    {
        super(m, n);
    }

    String mMailContent   = null;


    Message mMessage;

    public void setMail(String m)
    {
        mMailContent = m;
    }

    public String getMail()
    {
        return mMailContent;
    }
/*
    public String getAttachmentName(int i)
    {
        return mAtts.get(i);
    }
*/
    public void setAttachmentDownload(int position){ mPosition = position; mDownload = mAtts.get(position);  }



    public String parseContent()
    {
        try
        {
          return parseContent(mMessage);
        }
        catch (MessagingException e) { catchME(e); }
        catch (IOException e) { e.printStackTrace(); }
        return null;
    }

    public void doIt()
    {
        super.doIt();
        try
        {

            store = session.getStore("imaps");
            store.connect();
            String fn;
            if(node.getNumber() != 0)
            {
                fn = (node.getFolderName() != null) ? node.getFolderName() : "INBOX";
                inbox = store.getFolder(fn);
                if(!inbox.isOpen()) inbox.open(Folder.READ_WRITE);
                mMessage = inbox.getMessage(node.getNumber());
                setMail(parseContent((Part) mMessage));
                // inbox.close(true);
            }
            else
            {
                // Loop through all folders
                Node cats = EpManager.getFolders();
                if (cats != null) for (int i = 0; i < cats.getSize(); i++)
                {
                    Node f = cats.getChild(i);
                    fn = f.getFolderName();
                    if(store.isConnected()) inbox = store.getFolder(fn);
                    while(inbox.isOpen()) this.wait(1000);
                    inbox.open(Folder.READ_ONLY);
                    Log.i("MailRead","Existing folder name " + node.getFolderName() + " and id " + node.getName());
                    Message[] messages = inbox.search(new HeaderTerm("Message-ID",node.getName()));
                    if(messages.length > 0)
                    {
                        Message message = messages[0];
                        setMail(parseContent((Part) message));
                        node.setFolderName(fn);

                        Node mn = EpManager.getNode(fn);
                        // if (BuildConfig.DEBUG) { if (mn==null) throw new AssertionError("Cannot find folder " + fn);}
                        if (mn == null)  // Create mother folder node
                            mn = EpManager.createNode(null, fn, BoxFragment.class, false);
                        inMail(mn, message);
                        inbox.close(true);
                        break;
                    }
                    inbox.close(true);
                }
            }
            EpManager.handleState(this, 3);
            if(!isFinised()) inChildren(node,node.getName());
            store.close();
            setFinished();
            EpManager.handleState(this, 4);
        }
        catch (MessagingException e) { catchME(e); }
        catch (IOException | InterruptedException | IllegalStateException e) { e.printStackTrace(); }
    }

    public void postIt(int what) throws InstantiationException, IllegalAccessException
    {
        super.postIt(what);

        if(what == 3)
        {
            EpFragment fragment = (EpFragment) node.getFragment();
            fragment.paint(node);
            EpManager.fixButtons();
        }
        else if(what == 8)
        {
            EpFragment fragment = (EpFragment) node.getFragment();
            int index = mAtts.indexOf(mDownload);
            String type = mAttach.get(index).getType();
            Uri uri = mAttach.get(index).getPath();
            Log.i("Mail read ","attachment in uri " + uri.toString());
            Log.i("Mail read ","with type " + type);
            //  File file = new File(dir,mDownload);
            //Uri uri = FileProvider.getUriForFile(main,BuildConfig.APPLICATION_ID + ".provider",dir);

            Intent openintent = new Intent(Intent.ACTION_VIEW);
            openintent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            openintent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            openintent.setDataAndType(uri, type);
            try
            {
                //Log.i("read mail","start activity " + uri.toString());
                fragment.startActivity(openintent);
            }
            catch (ActivityNotFoundException e)
            {
                e.printStackTrace();
            }
        }

    }

}
