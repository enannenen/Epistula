package no.akerbaek.epistula;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class EpNodeButton extends androidx.appcompat.widget.AppCompatButton
{
    int a=255,r=0,g=220;

    public EpNodeButton(@NonNull Context context)
    {
        super(context);
    }

    public EpNodeButton(@NonNull Context context, @Nullable AttributeSet attrs)
    {
        super(context, attrs);
    }

    public EpNodeButton(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }


    void setRecent(Boolean recent)
    {
        if(recent)
        {
            r = 200;
            g = 0;
        }
        else
        {
            r = 0;
            g = 120;
        }
        setTextColor(Color.argb(a,r,g,0));
    }

    @Override
    public void setEnabled(boolean enabled)
    {
        super.setEnabled(enabled);
        if(enabled) a = 255;
        else  a = 50;
        setTextColor(Color.argb(a,r,g,0));
    }
}

