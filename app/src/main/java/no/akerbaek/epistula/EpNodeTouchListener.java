package no.akerbaek.epistula;

import android.graphics.Color;
import android.text.Layout;
import android.text.Selection;
import android.text.Spannable;
import android.text.style.BackgroundColorSpan;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewParent;
import android.widget.AdapterView;
import android.widget.TextView;

import static java.lang.Math.abs;
import static java.lang.Math.max;

class EpOnTouchListener implements View.OnTouchListener
{

    static float px,py;
    static float ppx,ppy;

    //    @SuppressLint("ClickableViewAccessibility")

    int k = Color.rgb(50, 250, 250);
    BackgroundColorSpan span = new BackgroundColorSpan(k);
    boolean selecting = false;
    int start; int stop;

    public boolean onLeft(View v, MotionEvent event)
    {
        Log.i("Listwiew ontouch", "swipe left ");
        return true;
    }

    public boolean onRight(View v, MotionEvent event)
    {
        Log.i("Listwiew ontouch", "swipe right ");
        return true;
    }

    public boolean onUp(View v, MotionEvent event)
    {
        Log.i("Listwiew ontouch", "swipe up ");
        return true;
    }

    public boolean onDown(View v, MotionEvent event)
    {
        Log.i("Listwiew ontouch", "swipe down ");
        return true;
    }


    public boolean onClick(View v, MotionEvent event)
    {
        Log.i("Listwiew ontouch", "click ");

        return false;
    }


    public boolean onLongClick(View v, MotionEvent event)
    {
        Log.i("Listwiew ontouch", "longclick ");
        return false;
    }

    public  boolean onSelect(View v,  MotionEvent event)
    {
        Log.i("Listwiew ontouch", "select ");
        return false;
    }

    public boolean onTouch(View v, MotionEvent event)
    {

        float swDist = v.getWidth() / 2;
        float shDist = v.getHeight() / 2;
        // Interpret MotionEvent data

        // Handle touch here

        // Log.i("TouchListener","OnTouch action" + event.getAction());
        if(event.getAction() == MotionEvent.ACTION_DOWN)
        {
            Log.i("Listwiew ontouch", "action down:  event x: "+event.getX()+" event y: " + event.getY());
            ppx = px = event.getX();
            ppy = py = event.getY();
            selecting = false;

            return true;
        }
        else if(event.getAction() == MotionEvent.ACTION_MOVE)
        {
            float dx = abs(px - event.getX());
            float dy = abs(py - event.getY());
            //if (event.getEventTime() - event.getDownTime() > 700 && dx < 3 && dy < 3)
            //    return onLongClick(v, event);
            px = event.getX();
            py = event.getY();


            if(v instanceof TextView)
            {
                ViewParent parent = v.getParent();

                float ldx = abs(ppx - event.getX());
                float ldy = abs(ppy - event.getY());
                float ld = max(ldx, ldy);
                float lt = abs(event.getEventTime() - event.getDownTime());
                double speed = ld / (lt + 0.0001);
                //Log.i("TouchListener","Speed " + speed);
                if(speed < 0.2)
                {
                    if (parent != null &&  !selecting)
                        parent.requestDisallowInterceptTouchEvent(true);

                    Layout layout = ((TextView) v).getLayout();
                    int line = layout.getLineForVertical((int)ppy);
                    start = layout.getOffsetForHorizontal(line, ppx);
                    line = layout.getLineForVertical((int)event.getY());
                    stop = layout.getOffsetForHorizontal(line, event.getX());

                    ((Spannable) ((TextView) v).getText()).setSpan(span, Math.min(start, stop), Math.max(start,stop), Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
                    selecting = true;
                    //Log.i("TouchListener","Speed slow");
                }
                else if(speed > 0.7)
                {
                    if (parent != null && selecting)
                        parent.requestDisallowInterceptTouchEvent(false);
                    ((Spannable) ((TextView) v).getText()).removeSpan(span);
                    selecting = false;
                    //Log.i("TouchListener","Speed fast");
                }

                return true;
            }
            if(dx > dy)
                return true;
        }
        else if(event.getAction() == MotionEvent.ACTION_UP)
        {
            Log.i("Listwiew ontouch", "action up:  event x: "+event.getX()+" event y: " + event.getY());

            float dx = abs(ppx - event.getX());
            float dy = abs(ppy - event.getY());
            if(selecting && (start != stop))
            {
                Selection.setSelection((Spannable) ((TextView) v).getText(),start, stop);
                return onSelect(v, event);
            }
            if (event.getEventTime() - event.getDownTime() > 700 && dx < 3 && dy < 3)
                return onLongClick(v, event);
            if(dx < 4 && dy < 4)
            {
                if(v instanceof TextView) ((Spannable) ((TextView) v).getText()).removeSpan(span);
                selecting = false;
                v.performClick();
                return onClick(v, event);
            }
            if(dx > dy*2 && dx > swDist)
            {
                Log.i("Listwiew ontouch", "swipe horizontal :  event x: "+event.getX()+" prev x: " + ppx);
                if(ppx > event.getX())
                    return onLeft(v,event);
                else
                    return onRight(v,event);
            }
            if(dy > dx*2 && dy > shDist)
            {
                Log.i("Listwiew ontouch", "swipe vertical :  event y: "+event.getY()+" prev y: " + ppy);
                if (!v.canScrollVertically(1) &&  ppy > event.getY())
                {
                    // bottom of scroll view
                    return onUp(v, event);  // Want to move further down
                }
                else if (!v.canScrollVertically(-1) &&  event.getY() > ppy)
                {
                    // top of scroll view
                    return onDown(v, event);// Want to move further up
                }


            }

        }
        else if(event.getAction() == MotionEvent.ACTION_CANCEL)
        {
            return true; //  action cancel
        }
        // Log.i("Listwiew ontouch", "dump event " + event.getX());




        return false;

    }


}

public class EpNodeTouchListener extends EpOnTouchListener  implements View.OnTouchListener
{
    private final Node node;

    public EpNodeTouchListener(Node n)
    {
        node = n;
    }

    public boolean onLeft(View v, MotionEvent event)
    {
        Log.i("OnTouchListener", "swipe left ");
        Node next = EpManager.getNextNode();

        if (next == null)
            return false;

        EpManager.navigateTo(next);

        return true;
    }

    public boolean onRight(View v, MotionEvent event)
    {
        Log.i("setOnTouchListener", "swipe right ");
        Node p = EpManager.getPrevNode();

        if (p == null)
            return false;

        EpManager.navigateTo(p);

        return true;
    }

    public boolean onDown(View v, MotionEvent event)
    {
        Log.i("setOnTouchListener", "swipe down ");

        if (node.getEnd() > 1) try
        {
            node.setCalendar(null);
            node.getFragment().reload();
            return false;
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        Node mother = EpManager.getCurrentMother();
        if (mother == null)
            return false;
        mother.setChildIndex(node);  // Remember where we came from

        EpManager.navigateTo(mother.getMother(), mother);


//        return false;
        return true;
    }

    public boolean onUp(View v, MotionEvent event)
    {
        Log.i("setOnTouchListener", "swipe up ");
        Node c = node.getActiveChild();
        if (c == null)
            return false;
        c.setMotherIndex(node);

        EpManager.navigateTo(node, c);
        return true;
    }
    /*
    public boolean onClick(View v, MotionEvent event)
    {
        Log.i("setOnTouchListener", "click ");

        EpTextView text = (EpTextView)(v.findViewById(R.id.content));
        text.setTextIsSelectable(true);
        return true;
    }

     */

}

