package no.akerbaek.epistula;


import android.text.Layout;
import android.text.Selection;
import android.text.Spannable;
import android.text.method.BaseMovementMethod;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import static java.lang.Math.abs;

/**
 * A movement method that traverses links in the text buffer and fires clicks. Unlike
 * {@link LinkMovementMethod}, this will not consume touch events outside {@link ClickableSpan}s.
 */
public class EpLinkMovementMethod extends BaseMovementMethod
{
    static float ppx,ppy;
    float pt;
    float   px;

    private static EpLinkMovementMethod sInstance;

    public static EpLinkMovementMethod getInstance()
    {
        if (sInstance == null)
        {
            sInstance = new EpLinkMovementMethod();
        }
        return sInstance;
    }

    @Override
    public boolean canSelectArbitrarily()
    {
        return true;
    }
/*
    private View getParentTouchable(View view)
    {
        try
        {
            if (((View) view.getParent()).isInTouchMode())
            {
                return (View) view.getParent();
            }
            else
            {
                return getParentTouchable(((View) view.getParent()));
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
*/
    @Override
    public boolean onTouchEvent(TextView widget, Spannable buffer, MotionEvent event)
    {
        View parent = (View) widget.getParent();

        int action = event.getAction(); // Masked();
        if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_DOWN)
        {

            int x = (int) event.getX();
            int y = (int) event.getY();
            x -= widget.getTotalPaddingLeft();
            y -= widget.getTotalPaddingTop();
            x += widget.getScrollX();
            y += widget.getScrollY();

            Layout layout = widget.getLayout();
            int line = layout.getLineForVertical(y);
            int off = layout.getOffsetForHorizontal(line, x);

            ClickableSpan[] link = buffer.getSpans(off, off, ClickableSpan.class);
            if (link.length > 0)
            {
                if (action == MotionEvent.ACTION_UP)
                {
                    float dx = abs(ppx - x);
                    float dy = abs(ppy - y);

                    if (dx < 4 && dy < 4 && event.getEventTime() - event.getDownTime() < 400 )
                    {
                        link[0].onClick(widget);
                        //widget.setClickable(false);
                        return true;
                    }

                    //
                }
                else
                {
                    ppx = x;
                    ppy = y;
                    Selection.setSelection(buffer, buffer.getSpanStart(link[0]),
                            buffer.getSpanEnd(link[0]));
                    //widget.setClickable(true);

                    return true;
                }
            } else
            {
                Selection.removeSelection(buffer);
                Log.i("LinkMovement touch", "Link not pressed");
            }
        }
        else
        {
            float time = (event.getEventTime() - pt );
            float dist = (event.getX() - px);
            float speed = abs(dist) / time;


            pt = event.getEventTime();
            px = event.getX();
            Log.i("LinkMovement touch", "speed " + speed);
        }
        return false; //.onTouchEvent(event);
    }

    @Override
    public void initialize(TextView widget, Spannable text)
    {
        Selection.removeSelection(text);
    }
}