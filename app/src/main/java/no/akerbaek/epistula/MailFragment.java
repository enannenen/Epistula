package no.akerbaek.epistula;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.app.Activity.RESULT_OK;

// TODO: Attachment list only partially visible
/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MailFragment extends EpMessageFragment implements EpFragmentInterface
{
    private static final String ARG_NAME = "name";

    View view;
    String mCacheMail;
    // String[] mAtts;

    public MailFragment()
    {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param name Parameter 1.

     * @return A new instance of fragment MailFragment.
     */
    public static MailFragment newInstance(String name)
    {
        MailFragment fragment = new MailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_NAME, name);
        fragment.setArguments(args);
        return fragment;
    }

    private static final Pattern urlPattern = Pattern.compile(
            "(?:^|[\\W])((ht|f)tp(s?):\\/\\/)"
                    + "(([\\w\\-]+\\.){1,}?([\\w\\-.~]+\\/?)*"
                    + "[\\p{Alnum}.,%_=?&#\\-+()\\[\\]\\*$~@!:/{};']*)",
            Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);

    private SpannableString markLinks(String s)
    {
        SpannableString ret = new SpannableString(s);
        Matcher matcher = urlPattern.matcher(ret);
        while (matcher.find())
        {
            int ms = matcher.start(1);
            int me = matcher.end();
            final String url = ret.subSequence(ms,me).toString();
            ClickableSpan cs = new ClickableSpan()
            {
                @Override
                public void onClick(View textView)
                {
                    Log.i("MailRead", "Clicked url");
                    Uri webpage = Uri.parse(url);
                    Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                    if (intent.resolveActivity(EpManager.getContext().getPackageManager()) != null)
                    {
                        startActivity(intent);
                    }
                }
            };
            ret.setSpan(cs, ms, me, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        }
        return ret;
    }

    @Override
    public void onSaveInstanceState(@NonNull final Bundle outState)
    {
        super.onSaveInstanceState(outState);
        if(mMailReader != null && mMailReader.getMail() != null)
            mCacheMail = mMailReader.getMail();
        outState.putString(ARG_TEXT,mCacheMail);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null)
        {
            //probably orientation change
            mCacheMail = savedInstanceState.getString(ARG_TEXT);
        }
        /*
        if (getArguments() != null)
        {
            String name = getArguments().getString(ARG_NAME);
            if(node == null)
                node = EpManager.getNode(name);
        }
        */
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.frag_mail, container, false);

        View mv = view.findViewById(R.id.mail);
        mv.setOnTouchListener(new EpNodeTouchListener(node)
        );
        final ScrollView envelope = (ScrollView)mv; // view.findViewById(R.id.mail);
        envelope.setOnTouchListener(new EpNodeTouchListener(node)
        );

        final TextView tv = view.findViewById(R.id.content);
        //tv.setMovementMethod(EpLinkMovementMethod.getInstance());

        tv.setOnTouchListener(new EpNodeTouchListener(node)
        {
            ActionMode mmActionmode;

            public boolean onClick(View v, MotionEvent event)
            {
                if(mmActionmode!=null) mmActionmode.finish();
                TextView widget = (TextView) v;
                Spannable buffer = (Spannable) widget.getText();

                int x = (int) event.getX();
                int y = (int) event.getY();
                x -= widget.getTotalPaddingLeft();
                y -= widget.getTotalPaddingTop();
                x += widget.getScrollX();
                y += widget.getScrollY();

                Layout layout = widget.getLayout();
                int line = layout.getLineForVertical(y);
                int off = layout.getOffsetForHorizontal(line, x);

                ClickableSpan[] link = buffer.getSpans(off, off, ClickableSpan.class);
                if (link.length > 0)
                {
                    link[0].onClick(widget);
                    return true;
                }

                Log.i("LinkMovement touch", "Link not pressed");
                return false;
            }

            // @Override
            public boolean onSelect(View v, MotionEvent e )
            {
                Log.i("mailfrag", "onselect ");

                final TextView textView = (TextView) v;

                mmActionmode = (ActionMode) v.startActionMode(null);

                //editMail(start,stop,R.id.button2);
                return true;
            }

        });

        tv.setTextIsSelectable(true);
        //tv.setMovementMethod(EpLinkMovementMethod.getInstance());
        tv.setClickable(true);
        tv.setLongClickable(true);
        tv.setFocusable(true);
        tv.setEnabled(true);
        tv.setFocusableInTouchMode(true);

        /*
        //envelope.setOnLongClickListener(new View.OnLongClickListener()
        {
            // @Override
            public boolean onLongClick(View v, MotionEvent e )
            {
                 Log.i("setOnLongClickListener", "click ");

                 int[] loc = new int[2];
                textView.getLocationOnScreen(loc);
                int pos = textView.getOffsetForPosition(e.getRawX()-loc[0], e.getRawY()-loc[1]);
                Log.i("onlongclick"," touch y " + e.getRawY() + " view y " + loc[1]);
                int stop  = textView.getText().toString().indexOf("\n", pos);
                int start = textView.getText().toString().lastIndexOf("\n", pos) +1;
                if(start<0) start=0;
                if(stop<0)  stop=textView.getText().length();
                textView.setTextIsSelectable(true);
                Selection.setSelection((Spannable)textView.getText(),start,stop);

                //textView.setSelected(true);
                // textView.initActionMode();

                // textView.performLongClick(e.getX(), e.getY());

                return true;
            }


        });

*/
        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view,savedInstanceState);
        Log.i("MailFragment","view created");
        TextView name = (view.findViewById(R.id.name));
        name.setText(node.getTitle());
        TextView from =  (view.findViewById(R.id.from));
        LinearLayout fromR = (view.findViewById(R.id.fromr));
        String fv = node.getFrom();
        if(fv != null)
            from.setText(fv);
        else
            fromR.setVisibility(View.GONE);

        final MailFragment current=this;
        Button mailButton =  (view.findViewById(R.id.reply));
        mailButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.i("MailFragment","reply button clicked");
                current.editMail(0,0, R.id.button2);
            }
        });

        Button archiveButton =  (view.findViewById(R.id.archive));
        archiveButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.i("MailFragment","archive button clicked");
                current.archiveMail();
            }
        });

        Button trashButton =  (view.findViewById(R.id.delete));
        trashButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.i("MailFragment","trash button clicked");
                current.deleteMail();
            }
        });

        if(mMailReader==null)
        {
            mMailReader = new EpMailRead(EpManager.getContext(), node);
        }
        // if(sample.isRunning()) ;
        if(mMailReader.isFinised())
        {
            Log.i("MailFragment","create adapter");

            paint(node);
        }
        else
            EpManager.startRequest(mMailReader, EpManager.getContext());

        ListView attView = mView.findViewById(R.id.attView);
        EpAttachmentVectorAdapter arrayAdapter = new EpAttachmentVectorAdapter(EpManager.getContext(), R.layout.list_attach, R.id.attView, mMailReader.mAtts, mMailReader.mAttach, this);
        attView.setAdapter(arrayAdapter);
        ArrayAdapter<String> a = (ArrayAdapter<String>)attView.getAdapter();
        if(a != null) a.notifyDataSetChanged();
        setListViewHeightBasedOnChildren(attView);

    }

    private void addAttachments()
    {
        /*
        if(attView.getChildCount()==0)
          for (int pos = 0; pos < sample.mAtts.size(); pos++)
          {
            TextView nv = new TextView(EpManager.getContext());
            nv.setText(sample.getAttachmentName(pos));
            attView.addView(nv);
          }

         */
    }

    public Boolean paint(Node n)
    {
        boolean painted = true;
        // TODO: Fragment should be connected to a context
        String ml = EpManager.getContext().getString(R.string.lost);
        if(mMailReader.getMail() != null)
            ml = mMailReader.getMail();
        else if(mCacheMail != null)
            ml = mCacheMail;
        else
            painted = false;

        if(view != null)
        {
            final EpTextView text;
            text = (view.findViewById(R.id.content));

            SpannableString spannable = markLinks(ml);
            // Selection.setSelection(spannable, spannable.length());
            //text.setOnLongClickListener(new EpOnLongClickListener());
            addAttachments();
            ListView attView =  mView.findViewById(R.id.attView);
            ArrayAdapter<String> a = (ArrayAdapter<String>)attView.getAdapter();
            setListViewHeightBasedOnChildren(attView);
            if(a != null) a.notifyDataSetChanged();
            text.setText(spannable, TextView.BufferType.SPANNABLE);
            //text.setTextIsSelectable(true);
            //text.setClickable(true);
            //text.setMovementMethod(LinkMovementMethod.getInstance());

        }
        return painted;
    }

    private String reFix(String s)
    {
 //       String re = s.replaceAll("̈́.","T");
        String re = s;
        int l;
        Pattern p = Pattern.compile("^[A-Za-z]{2}: ");
        do
        {
            l = re.length();
            re = p.matcher(re).replaceAll("");
        }while (re.length() != l);
        return "Re: " + re;
    }

    @Override
    public void editMail(int start, int stop, int type)
    {
        if(view == null) return;
        EpTextView contentView = view.findViewById(R.id.content);
        String content = contentView.getText().toString();
        String name = EpManager.createId();
        Node edNode = EpManager.createNode(EpManager.getCurrentNode(), name, EditFragment.class, true);
        edNode.setFrom(node.getFrom());  // Actually the receiver of new message
        String t = node.getTitle();
        edNode.setTitle(reFix(t));
        edNode.setMotherId(node.getName());
        Bundle args = new Bundle();
        args.putString(ARG_NAME, name);
        args.putString(ARG_TEXT, content);
        args.putInt(ARG_START, start);
        args.putInt(ARG_END, stop);
        args.putInt(ARG_TYPE, type);


        edNode.setArguments(args);

        EpManager.navigateTo(edNode);
    }

    public void archiveMail()
    {
        int year = node.getCalendar().get(Calendar.YEAR);
        EpMailMover mover = new EpMailMover(EpManager.getContext(), node, "Archives/" + year);
        EpManager.startRequest(mover, EpManager.getContext());
    }

    public void deleteMail()
    {
        EpMailMover mover = new EpMailMover(EpManager.getContext(), node, "Trash");
        EpManager.startRequest(mover, EpManager.getContext());
    }


    public void reload()
    {
        if(mMailReader==null)
        {
            mMailReader = new EpMailRead(EpManager.getContext(), node);
        }
        if(!mMailReader.isRunning())
            EpManager.startRequest(mMailReader, EpManager.getContext());
    }

    /*
    private String getFolderPath(String path)
    {
        final String[] split = path.split(":");//split the path.
        return split[split.length-1].substring(0, split[split.length-1].lastIndexOf('/')+1);
    }
*/
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 123 && resultCode == RESULT_OK)
        {
            // String name = msample.mAtts.get(msample.mPosition);
            Uri uri = data.getData(); //The uri with the location of the file

            mMailReader.mAttach.get(mMailReader.mPosition).setPath(uri);
            Log.i("onActivityResult", "Attachment  save to " + uri.toString());
            reload();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults)
    {
        switch (requestCode)
        {
            case 125:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
                {
                    // Permission is granted. Continue the action
                    Intent intent;
                    intent = new Intent()
                                .setType(mMailReader.mAttach.get(mMailReader.mPosition).getType())
                                .putExtra(Intent.EXTRA_TITLE, mMailReader.mAtts.get(mMailReader.mPosition))
                                .setAction(Intent.ACTION_CREATE_DOCUMENT);

                    startActivityForResult(Intent.createChooser(intent, "Select a file"), 123);
                }
                else
                {
                    // Explain to the user that the feature is unavailable because
                    // the features requires a permission that the user has denied.
                    // At the same time, respect the user's decision. Don't link to
                    // system settings in an effort to convince the user to change
                    // their decision.
                    EpManager.alert(getString(R.string.unavailable));
                }

        }
        // Other 'case' lines to check for other
        // permissions this app might request.
    }
}
