package no.akerbaek.epistula;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class BoxFragment extends EpListFragment implements EpFragmentInterface
{
    EpMailList sample;

    public BoxFragment()
    {
        sample = null;
    }
    public static BoxFragment newInstance(String name)
    {
        BoxFragment fragment = new BoxFragment();
        Bundle args = new Bundle();
        args.putString(ARG_NAME, name);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view,savedInstanceState);
        //TextView boxName = (TextView)(view.findViewById(R.id.boxName));
        //boxName.setText(node.getName());

        if(mView != view) Log.i("BoxFragment","onViewCreated view inconsistent");

        Button mailButton =  (view.findViewById(R.id.compose));
        final BoxFragment current=this;
        mailButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.i("BoxFragment","compose button clicked");

                current.editMail(0,0,R.id.button2);
            }
        });

        if(sample==null)
            sample = new EpMailList(EpManager.getContext(), node);
        //noinspection StatementWithEmptyBody
        if(sample.isRunning())
            ;
        else if(sample.isFinised())
            paint(node);
        else
            EpManager.startRequest(sample, EpManager.getContext());

    }

    @Override
    public void onResume(){
        super.onResume();
        paint(node);

    }

    @Override
    public void editMail(int start, int stop, int type)
    {
        String content = "";
        String name = EpManager.createId();
        Node edNode = EpManager.createNode(EpManager.getNode("Drafts"), name, EditFragment.class, true);
        String b = EpManager.getEmailAddress(node.getName());  // Repack with only address part
        if(b == null)
            edNode.setFrom("tord@akerbaek.no");
        else
            edNode.setFrom(b);  // Actually the receiver of new message
        String t = "";
        edNode.setTitle(t);
        //edNode.setMotherId(node.getName());
        Bundle args = new Bundle();
        args.putString(ARG_NAME, name);
        args.putString(ARG_TEXT, content);
        args.putInt(ARG_START, start);
        args.putInt(ARG_END, stop);
        args.putInt(ARG_TYPE, type);
        edNode.setArguments(args);

        EpManager.navigateTo(edNode);
    }

    @Override
    public void reload()
    {
        if(sample==null)
            sample = new EpMailList(EpManager.getContext(), node);
        EpManager.startRequest(sample, EpManager.getContext());
    }

    public Boolean paint(Node n)
    {

        ListView simpleList;
        if(mView != null)
        {
            Log.i("BoxFragment.paint","view found in " +Thread.currentThread());
            if(node != n) Log.i("BoxFragment.paint","node inconsistent");

            simpleList = (ListView) (mView.findViewById(R.id.boxView));
            NodeVectorAdapter arrayAdapter = (NodeVectorAdapter)simpleList.getAdapter();
            arrayAdapter.notifyDataSetChanged();
            //simpleList.setSelection(arrayAdapter.getCount()-1);

/*
            NodeVectorAdapter arrayAdapter = new NodeVectorAdapter(EpManager.getContext(), R.layout.activity_box, R.id.textView, node);
            simpleList.setAdapter(arrayAdapter);
            simpleList.setSelection(arrayAdapter.getCount() - 1);
 */
            // TODO: Make sure that fragment has context or use manager.context
            EpManager.alert(getString(R.string.messagesin) + " " + node.getTitle() +" : " + node.getSize());
        }
        return true;
     }


}
