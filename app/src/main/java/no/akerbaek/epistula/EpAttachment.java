package no.akerbaek.epistula;

import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import javax.activation.DataHandler;
import javax.mail.BodyPart;
import javax.mail.Part;

public class EpAttachment
{
    String mName;
    Uri mPath;
    String mType;
    BodyPart mPart;
    int mMode;

    public EpAttachment(String name, Uri path, String type, int mode)
    {
      mName = name;
      mPath = path;
      if(type != null)
      {
          String[] ta = type.split(";");
          mType = ta[0];
      }
      else
          mType = null;
      mMode = mode;
    }

    String setStoredValues()
    {
        String fileName;
        String mimeType;

        fileName = mPath.getLastPathSegment();

        if (!mPath.getScheme().equals("file"))
        {
            Cursor cursor = null;
            try
            {
                cursor = EpManager.getContext().getContentResolver().query(mPath, new String[]
                        {
                                MediaStore.Images.ImageColumns.DISPLAY_NAME,
                                MediaStore.Images.ImageColumns.MIME_TYPE
                        }, null, null, null);

                if (cursor != null && cursor.moveToFirst())
                {
                    fileName = cursor.getString(cursor.getColumnIndex(MediaStore.Images.ImageColumns.DISPLAY_NAME));
                    Log.d("setStoredValues", "name is " + fileName);
                    mName = fileName;
                    mimeType = cursor.getString(cursor.getColumnIndex(MediaStore.Images.ImageColumns.MIME_TYPE));
                    Log.d("setStoredValues", "type is " + mimeType);
                    if(mType == null) mType = mimeType;
                }
            } finally
            {
                if (cursor != null)
                {
                    cursor.close();
                }
            }
        }
        return fileName;

    }

    String getName()
    {
        return mName;
    }

    Uri getPath()
    {
        return mPath;
    }

    String getType()
    {
        return mType;
    }

    BodyPart getPart()
    {
        return mPart;
    }

    void setPath(Uri path)
    {
       mPath = path;
    }

    void setPart(BodyPart p) {mPart = p;}

    boolean isMode(int mode)
    {
        return ((mode & mMode) != 0);
    }

    void setMode(int mode)
    {
        mMode |= mode;
    }

    void clrMode(int mode)
    {
        mMode &= (mode ^ 0xffff);
    }

}
