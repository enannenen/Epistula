package no.akerbaek.epistula;

public class EpRunnable implements Runnable
{
    Mailrequest mRSample;
    EpRunnable(Mailrequest task)
    {
        mRSample = task;
    }

    @Override
    public void run()
    {
        /*
         *
         */
        // Moves the current Thread into the background
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);

 /*
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
*/

        /*
         * Stores the current Thread in the PhotoTask instance,
         * so that the instance
         * can interrupt the Thread.
         */
        if(mRSample.isRunning()) return;
        mRSample.setThread(Thread.currentThread());
        mRSample.setRunning();
        mRSample.doIt();

    }

}
