package no.akerbaek.epistula;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;

import androidx.preference.PreferenceManager;

import java.util.Arrays;
import java.util.Comparator;

public class EpCite extends ClickableSpan
{
    protected int type;
    protected String mCommentText;
    protected SpannableStringBuilder cites;
    private Clicker mClicker;

    @Override
    public void updateDrawState(TextPaint textPaint)
    {
        super.updateDrawState(textPaint);
        // if((flag & 1) == 1)
        //    textPaint.setColor(Color.CYAN);
        // else
        if(type==R.id.button1)
            textPaint.setColor(Color.RED);
        else if(type==R.id.button2)
            textPaint.setColor(Color.BLUE);
        textPaint.setUnderlineText(false);
    }

    @Override
    public void onClick(View v)
    {
        Log.i("EpCite.onclick","Span is clicked");
        mClicker.onClick(this);
    }

    public String getCiteText()
    {
        int s = cites.getSpanStart(this);
        int e = cites.getSpanEnd(this);
        if(e <= s) return null;
        String text = cites.subSequence(s,e).toString().trim().replaceAll("\\r\\n|\\r|\\n", "\n> ");
        return "\n> " + text;
    }

    public String getCommentText()
    {
        // int s = cites.getSpanStart(this);
        // int e = cites.getSpanEnd(this);
        // String text = cites.subSequence(s,e).toString().trim();

        return mCommentText;
    }

    private Boolean overlap(EpCite c)
    {
        if(c==null  ||  c==this)
            return false;
        if(cites.getSpanEnd(this) <= cites.getSpanStart(c) || cites.getSpanStart(this) > cites.getSpanEnd(c))
            return false;
        return true;

    }

    public EpCite getComment()
    {
        if(type == R.id.button2 && mCommentText != null)  // Span has a comment
            return this;
        /*
        int s = cites.getSpanEnd(this);
        int e = cites.length();
        EpCite [] spans = cites.getSpans(s,e,EpCite.class);
        if(spans.length == 0) return null;
        if(spans[0].type != R.id.button2) return null;
        if(cites.getSpanStart(spans[0]) > s+1) return null;
        return  spans[0];
         */
        return null;
    }

    /*
    public EpCite(EpCite q, String c)
    {
        super();
        cites = q.cites;
        mClicker = q.mClicker;
        type  = R.id.button2;
        int pos = cites.getSpanEnd(q);
        if(pos<0)pos=cites.length();
        cites.insert(pos,c);
        cites.setSpan(this,pos,pos+c.length(),Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
    }

    public void setText(String c)
    {
        int s = cites.getSpanStart(this);
        int e = cites.getSpanEnd(this);
        cites.replace(s,e,c);
        // cites.setSpan(this,pos,pos+c.length(),Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    }
     */

    public void setCommentText(String c)
    {
        if(c.isEmpty())
        {
            mCommentText = null;
            type = R.id.button1;
        }
        else
        {
            mCommentText = c;
            type = R.id.button2;
        }
     /*
        String s = c.trim();
        if(s.length() != 0)
            s = "\n\n" + c.trim() + "\n\n\n";
        else
            s = null;

        EpCite comment;
        if(type == R.id.button2)     // This span is a comment
            comment = this;
        else
            comment = getComment();  // This span may have a comment


        if(comment != null)          // This is a comment
            if(s != null)
                comment.setText(s);      // Replace content
            else
                comment.removeCite();
        else if(s != null)
            comment = new EpCite(this,s);  // Create new comment
        return comment;
      */
    }

    public void removeCite()
    {
        int ns = cites.getSpanStart(this);
        int ne = cites.getSpanEnd(this);
        cites.removeSpan(this);
        cites.delete(ns,ne);
    }

    public static EpCite getSalute(SpannableStringBuilder cs)
    {
        EpCite [] spans = cs.getSpans(0,cs.length(),EpCite.class);
        for (EpCite span : spans)
        {
            int s = cs.getSpanStart(span);
            int e = cs.getSpanEnd(span);
            if (s == 0 && e == 0 && span.type == R.id.button2)
                return span;
        }
        return null;

    }

    private static class Sortbystart implements Comparator<EpCite>
    {
        // Used for sorting in ascending order of
        // roll number
        public int compare(EpCite a, EpCite b)
        {
            int as = a.cites.getSpanStart(a);
            int bs = b.cites.getSpanStart(b);
            if(as != bs)
              return as - bs;
            int ae = a.cites.getSpanEnd(a);
            int be = b.cites.getSpanEnd(b);
            return ae - be;
        }
    }

    public static String getProducedText(SpannableStringBuilder cs, String h, EpCite start, EpCite stop)
    {
        EpCite [] spans = cs.getSpans(0,cs.length(),EpCite.class);
        StringBuilder ret = new StringBuilder();
        boolean first = true;
        Arrays.sort(spans, new Sortbystart());
        for (EpCite p : spans)
        {
            if (start != null)
            {
                if (start != p) continue;
                start = null;
                continue;
            }
            if (p.getCiteText() != null)
            {
                if (first)
                {
                    if (ret.length() > 0) ret.append("\n\n");
                    ret.append(h);
                }
                ret.append("\n").append(p.getCiteText()).append("\n");
                first = false;
            }
            if (stop != null && p == stop) break;
            if (p.type == R.id.button2)
                ret.append("\n").append(p.getCommentText()).append("\n");
        }
        if(stop == null)
        {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(EpManager.getContext());

            String sign = prefs.getString("sign","");
            if(sign.length() > 0)
            {
                ret.append("\n-- \n");
                ret.append(sign);
                ret.append("\n");
            }
        }
        return ret.toString();
    }

    public static String getProducedText(SpannableStringBuilder cs, String h)
    {
        return getProducedText(cs,h,null,null);
    }

    public EpCite(SpannableStringBuilder cs, int  s, int e, Clicker c)
    {
        super();
        cites = cs;
        mClicker = c;
        type  = R.id.button1;


        if(s > e ) return;
        if(s == e)
            cites.setSpan(this,s,e, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        else
            cites.setSpan(this,s,e, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        EpCite [] spans = cites.getSpans(0,cites.length(),EpCite.class);
        for(int i=spans.length-1;i>=0;i--)
        {
            if(this.overlap(spans[i]))
            {
                //if(spans[i].type == R.id.button1) // Red, citation
                cites.removeSpan(spans[i]);
                if (spans[i].type == R.id.button2 && spans[i].getCommentText() != null) // Blue, comment
                {
                    if(mCommentText==null) setCommentText(spans[i].getCommentText());
                    else if (e > cites.getSpanEnd(spans[i]))
                    {
                        setCommentText(spans[i].getCommentText() + "\n\n" + getCommentText());
                    }
                    else
                    {
                        setCommentText(getCommentText()  + "\n\n" +  spans[i].getCommentText());
                    }
                }
                /*
                else if (spans[i].type == R.id.button2) // Blue, comment
                {
                    int ns;
                    int ne;  int ex=0;
                    if(s<cites.getSpanStart(spans[i]))
                    {
                        ns = s;
                        ex++;
                    }
                    else ns = cites.getSpanEnd(spans[i]);
                    if(e>cites.getSpanEnd(spans[i]))
                    {
                        ne = e;
                        ex++;
                    }
                    else ne = cites.getSpanStart(spans[i]);
                    if(ns>=ne)      // Trying to cite inside comment
                        cites.removeSpan(this);
                    else if(ex==2)  // Extends in both directions
                    {
                        spans[i].removeCite();
                    }
                    else            // Adjust citation to comment
                    {
                        cites.removeSpan(this);
                        cites.setSpan(this,ns,ne,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    }

                }
                */
            }
        }
    }
}