package no.akerbaek.epistula;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;

import static android.app.Activity.RESULT_OK;


interface Clicker
{
    void onClick(EpCite c);
}


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditFragment extends EpMessageFragment implements EpFragmentInterface
{
    Spannable content;
    SpannableStringBuilder spanned;

    ScrollView mEnvelope;
    View     mEdit;
    TextView mContent;
    TextView mFrom;
    EditText mTo;
    EditText mSubject;
    TextView mName;
    TextView mhcontact;

    public EditFragment()
    {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param args A list of arguments to initiate editing a message

     * @return A new instance of fragment EditFragment.
     */
    public static EditFragment newInstance(Bundle args)
    {
        EditFragment fragment = new EditFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onSaveInstanceState(@NonNull final Bundle outState)
    {
        super.onSaveInstanceState(outState);
        spanCode(spanned, outState);
    }
/*
    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
    }
*/
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (savedInstanceState != null)
        {
            //probably orientation change
            spanned = (SpannableStringBuilder) spanDecode(savedInstanceState);
        }
        else if (args != null)
        {
            String name = args.getString(ARG_NAME);
            if(node == null)
                node = EpManager.getNode(name);

                content = new SpannableString(args.getString(ARG_TEXT));
                // spanned = new SpannableStringBuilder(args.getString(ARG_TEXT));

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.frag_edit, container, false);


        return mView;
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view,savedInstanceState);

        mEnvelope = (ScrollView) mView.findViewById(R.id.envelope);
        mEnvelope.setOnTouchListener(new EpNodeTouchListener(node));

        mSubject = (EditText) (view.findViewById(R.id.subject));
        mName = (TextView) (view.findViewById(R.id.name));
        mSubject.setText(node.getTitle());
        mName.setText(node.getTitle());

        mContent = view.findViewById(R.id.content);
        mEdit = view.findViewById(R.id.editpart);

        mFrom = (TextView) (view.findViewById(R.id.from));
        mTo = (EditText) (view.findViewById(R.id.to));

        String fv = node.getFrom();
        if(fv != null)
        {
            mFrom.setText(fv);
            mTo.setText(fv);
        }

        Button mailButton =  (view.findViewById(R.id.reply));
        mailButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.i("EditFragment","write button clicked");

                EditText comment = (EditText) (view.findViewById(R.id.comment));
                EpCite c = EpCite.getSalute(spanned);
                if(c != null)
                    showEditor(c, comment);
                else
                    editMail(0,0,R.id.button2);

            }
        });
        mFrom.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.i("EditFragment","contact text clicked");
                showEditor(null, mTo);
            }
        });
        mhcontact =  (view.findViewById(R.id.hcontact));
        mhcontact.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.i("EditFragment","contact header clicked");
                showEditor(null, mTo);
            }
        });
        mName.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.i("EditFragment","subject text clicked");
                showEditor(null, mSubject);
            }
        });
        TextView hsubject =  (view.findViewById(R.id.hsubject));
        hsubject.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.i("EditFragment","subject header clicked");
                showEditor(null, mSubject);
            }
        });

        /*
        mTo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.i("EditFragment","contact edit clicked");

                EpCite c = EpCite.getSalute(spanned);
                if(c!=null)
                    showEditor(c);
                else
                    editMail(0,0,R.id.button2);
                mTo.requestFocus();
            }
        });
         */

        Button sendButton =  (view.findViewById(R.id.send));
        sendButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.i("EditFragment","send button clicked");
                hideEditor(mEditCite);

                String mailContent = EpCite.getProducedText(spanned,node.getSubtitle()+ " " + getResources().getString(R.string.wrote) + ":");
                mContent.setText(mailContent);
                node.setFrom(mTo.getText().toString());
                node.setTitle(mSubject.getText().toString());
                // if(mSample != null) mSample.setAttachmentForward();
                // if(mSample != null) mSample.parseContent();
                EpMailSend sample = new EpMailSend(EpManager.getContext(),node);
                sample.setMail(mailContent);
                sample.setAttachments(mMailReader);
                EpManager.startRequest(sample, EpManager.getContext());
            }
        });

        EpTextView contentView = view.findViewById(R.id.content);
        Button viewButton =  (view.findViewById(R.id.view));
        viewButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.i("EditFragment", "edit background clicked");
                //v.setVisibility(View.GONE);
                mhcontact.setText(getResources().getString(R.string.from));
                hideEditor(mEditCite);
            }
        });

        Button addButton =  (view.findViewById(R.id.add));
        addButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.i("EditFragment","add att button clicked");

                Intent intent = new Intent()
                        .setType("*/*")
                        .setAction(Intent.ACTION_GET_CONTENT);


                startActivityForResult(Intent.createChooser(intent, "Select a file"), 124);


            }
        });


        Bundle args = getArguments();
        if (args!= null)
        {
            if(spanned == null)
            {
                spanned = new SpannableStringBuilder(args.getString(ARG_TEXT));
                editMail(args.getInt(ARG_START),args.getInt(ARG_END),args.getInt(ARG_TYPE));
            }
            else
                update();

        }

        contentView.setTextIsSelectable(true);
        contentView.setClickable(true);
        contentView.setMovementMethod(LinkMovementMethod.getInstance());

        ListView attView = mView.findViewById(R.id.attView);
        EpAttachmentVectorAdapter arrayAdapter;
        // If we are not answering an existing mail, the attachment vectors should be empty.
        if(mMailReader != null)
          arrayAdapter = new EpAttachmentVectorAdapter(EpManager.getContext(), R.layout.list_attach, R.id.attView, mMailReader.mAtts, mMailReader.mAttach, this);
        else
            arrayAdapter = new EpAttachmentVectorAdapter(EpManager.getContext(), R.layout.list_attach, R.id.attView, new Vector<String>(), new Vector<EpAttachment>(), this);
        attView.setAdapter(arrayAdapter);
        setListViewHeightBasedOnChildren(attView);

    }

    static void spanCode(SpannableStringBuilder s, Bundle b)
    {
        b.putString(ARG_TEXT, s.toString());
        // TODO: Code-decode all spans into bundle intarrays
    }

    static SpannableStringBuilder spanDecode(Bundle b)
    {
        return new SpannableStringBuilder(b.getString(ARG_TEXT));
    }

    private void update()
    {
        final EpTextView contentView = mView.findViewById(R.id.content);
        contentView.setText(spanned);
    }

    private void hideEditor(EpCite e)
    {
        EditText commentView = mEdit.findViewById(R.id.comment);
        mEdit.setVisibility(View.GONE);
        mContent.setVisibility(View.VISIBLE);
        mTo.setVisibility(View.GONE);
        mFrom.setVisibility(View.VISIBLE);
        mSubject.setVisibility(View.GONE);
        mName.setVisibility(View.VISIBLE);

        mName.setText(mSubject.getText().toString());

        InputMethodManager imm = (InputMethodManager) EpManager.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(commentView.getApplicationWindowToken(), 0);
        if(e!=null)
        {
            String ct = commentView.getText().toString();
            e.setCommentText(ct);
            //e.clearFlag(0);
        }
        update();
    }

    EpCite mEditCite;

    private void showEditor(EpCite c, EditText f)
    {
        String t;
        EpCite d;
        hideEditor(mEditCite);
        EditText commentView = mEdit.findViewById(R.id.comment);
        if(f==null) f = commentView;
        f.requestFocus();
        if(f==mTo)
        {
            mTo.setVisibility(View.VISIBLE);
            mFrom.setVisibility(View.GONE);
        }
        else if(f==mSubject)
        {
            mSubject.setVisibility(View.VISIBLE);
            mName.setVisibility(View.GONE);
        }

        mEdit.setVisibility(View.VISIBLE);
        mContent.setVisibility(View.GONE);

        mhcontact.setText(getResources().getString(R.string.to));

        if(f==commentView)
        {
            if (c != null && (d = c.getComment()) != null && (t = d.getCommentText()) != null)
            {
                commentView.setText(t);
            }
            else
            {
                commentView.setText("");
            }
            commentView.setVisibility(View.VISIBLE);
        }
        else
            commentView.setVisibility(View.GONE);
            // mEditCite = d != null ? d : c;
            mEditCite = c;
            TextView over = mEdit.findViewById(R.id.over);
            String intro = node.getSubtitle() + " " + getResources().getString(R.string.wrote) + ":";
            String firstPart = EpCite.getProducedText(spanned, intro, null, mEditCite);
            over.setText(firstPart);
            if (!firstPart.isEmpty()) intro = "";
            TextView under = mEdit.findViewById(R.id.under);
            String secondPart = "";
            if(mEditCite != null)
                secondPart =  EpCite.getProducedText(spanned, intro, mEditCite, null);
            under.setText(secondPart);

            commentView.setOnFocusChangeListener(new View.OnFocusChangeListener()
            {
                @Override
                public void onFocusChange(View v, boolean hasFocus)
                {
                    // When focus is lost check that the text field has valid values.

                    //if (!hasFocus)
                    //{
                        //hideEditor(mEditCite);
                    //}
                }
            });

    }

    private class SpanClicker implements Clicker
    {
        private final EditText commentView;

        public SpanClicker(EditText cv)
        {
            commentView = cv;
        }

        @Override
        public void onClick(EpCite c)
        {
            Log.i("EditFragment.cite","clicked");
//            if(mClosing)
//                mClosing = false;
//            else
                showEditor(c, commentView);
        }

    }

    @Override
    public void editMail(int start, int stop, int type)
    {
        if(mMailReader == null)
        {
            try
            {
                if(EpMessageFragment.class.isAssignableFrom(node.getMother().getType()))
                    mMailReader = ((EpMessageFragment)node.getMother().getFragment()).mMailReader;
            } catch (IllegalAccessException e)
            {
                e.printStackTrace();
            }
        }

        if(mMailReader == null)
        {
            //Node d = EpManager.createNode(node,"dummy", MailFragment.class, false);
            mMailReader = new EpMailRead(EpManager.getContext(), null);
            mMailReader.mAtts = new Vector<String>();
            mMailReader.mAttach = new Vector<EpAttachment>();

        }

        final EditText commentView   = (EditText) (mView.findViewById(R.id.comment));

        final EpCite c = new EpCite(spanned, start, stop, new SpanClicker(commentView));
         // Open comment box
        if(type == R.id.button2)
        {
          showEditor(c, null);
        }
        update();
        if(spanned.length() == 0)
        {
            Button viewButton =  (mView.findViewById(R.id.view));
            viewButton.setVisibility(View.GONE);
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 124 && resultCode == RESULT_OK)
        {
            Uri uri = data.getData(); //The uri with the location of the file

            EpAttachment newElement = new EpAttachment(null,uri,data.getType(),2);  // bit 1
            String name = newElement.setStoredValues();
            mMailReader.mAtts.add(name);

            DataSource source = new FileDataSource(new File(EpManager.getContext().getCacheDir(),name));
            final byte[] sourceBytes = new byte[1000];
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            InputStream is1;
            OutputStream os1;
            try
            {
                is1 = (InputStream) EpManager.getContext().getContentResolver().openInputStream(uri);
                os1 = source.getOutputStream();
                messageBodyPart.setContent(null,newElement.getType());
                messageBodyPart.setDataHandler(new DataHandler(source));
                messageBodyPart.setFileName(name);

            } catch (IOException | MessagingException e)
            {
                os1 = null;
                is1 = null;
                e.printStackTrace();
            }

            final InputStream is = is1;
            final OutputStream os = os1;
            Runnable r = new Runnable()
            {
                @Override
                public void run()
                {
                    int read;
                    try
                    {
                        while (((read = is.read(sourceBytes)) > 0))
                            os.write(sourceBytes, 0, read);
                    } catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }
            };
            if(os != null && is != null) new Thread(r).start();

            newElement.setPart(messageBodyPart);
            mMailReader.mAttach.add(newElement);

            ListView attView = mView.findViewById(R.id.attView);
            ArrayAdapter<String> a = (ArrayAdapter<String>)attView.getAdapter();
            setListViewHeightBasedOnChildren(attView);
            if(a != null) a.notifyDataSetChanged();
            Log.i("onActivityResult", "Attachment  load from " + uri.toString());
            reload();
        }
    }
    // Editfragment needs no paint since no content is loaded
}