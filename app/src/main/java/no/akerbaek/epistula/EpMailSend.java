package no.akerbaek.epistula;

import android.content.SharedPreferences;
import android.util.Log;


import androidx.preference.PreferenceManager;

import java.io.IOException;
import java.util.Properties;
import java.util.Vector;

import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

class EpMailSend extends EpMailbase implements Mailrequest
{

    public EpMailSend(MainActivity m, Node n)
    {
        super(m, n);
    }

    String mailContent;


    public void setMail(String m)
    {
        mailContent = m;
    }

    public String getMail()
    {
        return mailContent;
    }

    public void setAttachments(EpMailRead mailReader)
    {
        if(mailReader != null)
        {
            mAttach = mailReader.mAttach;
            mAtts = mailReader.mAtts;
        }
        else
        {
            mAttach = new Vector<EpAttachment>();
            mAtts = new Vector<String>();
        }
    }

    public Vector<EpAttachment> getAttachments()
    {
        return mAttach;
    }

    boolean hasAttachment(int mode)
    {
        for (EpAttachment a:mAttach)
        {
            if(a.isMode(mode)) return true;
        }
        return false;
    }

    public void setAttachmentForward(){ mPosition = -1; mDownload = null;  }

    @Override
    public void doIt()
    {
        super.doIt(); // Establish session on our own
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(main); // main.getPreferences(Context.MODE_PRIVATE);

        String host = prefs.getString("smtp_host","");
        String port = prefs.getString("smtp_port","");
        final String user = prefs.getString("smtp_user","");// provide user name
        final String password = prefs.getString("smtp_pass","");// provide password

        String from = prefs.getString("your_address","nomen.nescio@akerbaek.no");

        String to = node.getFrom();  // TODO: use getContact

        //Get the session object
        Properties props = new Properties();
        props.put("mail.smtp.host", host);
        if(port !=null &&  !port.isEmpty())
            props.put("mail.smtp.port", port);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.ssl.enable", "true");


        Session sendSession = Session.getInstance(props,
                new Authenticator()
                {
                    protected PasswordAuthentication getPasswordAuthentication()
                    {
                        return new PasswordAuthentication(user, password);
                    }
                });
        // sendSession.setDebug(true);

        // Check if attachments should be forwarded
        if (hasAttachment(4))  // Forward attachment
        {
            // Read incoming message again
            // super.doIt();

            try
            {
                store = session.getStore("imaps");
                store.connect();
                Node mn = node.getMother();
                String fn = (mn.getFolderName() != null) ? mn.getFolderName() : "INBOX";
                inbox = store.getFolder(fn);
                if (!inbox.isOpen()) inbox.open(Folder.READ_WRITE);
                Message message = inbox.getMessage(mn.getNumber());
                setAttachmentForward();
                parseContent((Part) message);
            }
            catch (MessagingException e)
            {
                catchME(e);
                EpManager.handleState(this,6);
                return;
            }
            catch (IOException e)
            {
                e.printStackTrace();
                EpManager.handleState(this,6);
                return;
            }
        }


        //Compose the message
        try
        {

            MimeMessage message = new MimeMessage(sendSession) { @Override protected void updateMessageID() { } // Prevent MimeMessage from overwriting our Message-ID
            };
            message.setFrom(new InternetAddress(from));
            // TODO: Split to on comma and add each recepient separatly

            for (String s:to.split("[,; ]+"))
            {
                InternetAddress ia = new InternetAddress(s);
                if(s.contains("@"))
                    message.addRecipient(Message.RecipientType.TO, ia);
            }

            message.setSubject(node.getTitle());

            message.setHeader("Message-ID",node.getName());
            String irt = node.getMotherId();
            if(irt != null && irt.contains("@"))
                message.setHeader("In-Reply-To",irt);

            // message.setText(getMail());
            // Create the message part
            BodyPart messageBodyPart = new MimeBodyPart();

            // Fill the message
            messageBodyPart.setText(getMail());

            // Create a multipart message
            Multipart multipart = new MimeMultipart();

            // Set text message part
            multipart.addBodyPart(messageBodyPart);

            if(mAttach != null && (hasAttachment(2 | 4))) for (EpAttachment a:mAttach)
            {
                // Part two is attachment
                if(a.isMode(2|4))
                {
                    multipart.addBodyPart(a.getPart());
                    Log.i("MailSend","Added attached body part");
                }

            }
            message.setContent(multipart );
            //send the message
            Transport transport = sendSession.getTransport();
            transport.connect(user,password);
            transport.sendMessage(message, message.getAllRecipients());
            //Transport.send(message);

            Log.i("Sendmail.doit","message sent successfully...");

            // Copy message to "Sent Items" folder as read
            Store store = session.getStore("imaps");
            store.connect();
            /*
            host = prefs.getString("imap_host","");
            port = prefs.getString("imap_port","143");
            int p;
            if(port==null || port.isEmpty()) p = 143;
            else p = Integer.valueOf(port);
            store.connect(host, p, prefs.getString("imap_user",""), prefs.getString("imap_pass",""));
             */
            Folder folder = store.getFolder("Sent");
            if(!folder.isOpen()) folder.open(Folder.READ_WRITE);
            message.setFlag(Flags.Flag.SEEN, true);
            folder.appendMessages(new Message[] {message});
            store.close();

            Log.i("Sendmail.doit","message copied to Sent folder...");

        }
        catch (MessagingException e)
        {
            catchME(e);
            EpManager.handleState(this,6);
            return;
        }
        EpManager.handleState(this,5);
    }


    public void postIt(int what)
    {
        // super.postIt();    // What is Mailbase postIt doing? Currently nothing


        // delete fragment
        // remove node fragment

        // Change fragment type
        EpManager.reInitFragment(node, MailFragment.class);
        if(what==6)
            EpManager.navigateTo(EpManager.getSettings());
        EpManager.fixButtons();

    }

}
