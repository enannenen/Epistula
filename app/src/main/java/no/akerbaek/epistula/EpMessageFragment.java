package no.akerbaek.epistula;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

public class EpMessageFragment extends EpFragment implements EpFragmentInterface
{
  public EpMailRead mMailReader;



  protected  void setListViewHeightBasedOnChildren(final ListView listView)
  {
    ListAdapter listAdapter = listView.getAdapter();
    if (listAdapter == null)
    {
      Log.i("Compute height", "Return ");
      return;
    }

    ViewGroup.LayoutParams params = listView.getLayoutParams();

    Log.i("Compute height", "No of children " + listAdapter.getCount());
    int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
    listView.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
    int temp = listView.getMeasuredHeight();

    int totalHeight = 0;
    View view = null;
    for (int i = 0; i < listAdapter.getCount(); i++)
    {
      view = listAdapter.getView(i, view, listView);
      if (i == 0)
      {
        view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));
      }
      view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
      temp = view.getMeasuredHeight();
      if(temp > 200) temp = 200;
      totalHeight += temp;
      Log.i("Compute height", "Sum " + totalHeight);
    }

    params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
    listView.setLayoutParams(params);
    listView.requestLayout();
  }

}
