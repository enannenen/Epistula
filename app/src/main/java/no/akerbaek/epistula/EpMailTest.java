package no.akerbaek.epistula;


import android.content.SharedPreferences;

import androidx.preference.PreferenceManager;

import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;

public class EpMailTest extends EpMailbase implements Mailrequest
{
    public EpMailTest(MainActivity m, Node n)
    {
        super(m, n);
    }

    public void doIt()
    {
        super.doIt();
        try
        {
            if(node == null)
            {
                // Test read connection
                store = session.getStore("imaps");
                if (!store.isConnected()) store.close();
                store.connect();
                store.close();
            }
            else
            {
                // Test send connection
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(main); // main.getPreferences(Context.MODE_PRIVATE);

                String host = prefs.getString("smtp_host", "");
                String port = prefs.getString("smtp_port", "");
                final String user = prefs.getString("smtp_user", "");// provide user name
                final String password = prefs.getString("smtp_pass", "");// provide password

                //Get the session object
                Properties props = new Properties();
                props.put("mail.smtp.host", host);
                if (port != null && !port.isEmpty())
                    props.put("mail.smtp.port", port);
                props.put("mail.smtp.auth", "true");
                props.put("mail.smtp.ssl.enable", "true");

                Session sendSession = Session.getInstance(props,
                        new javax.mail.Authenticator()
                        {
                            protected PasswordAuthentication getPasswordAuthentication()
                            {
                                return new PasswordAuthentication(user, password);
                            }
                        });

                Transport transport = sendSession.getTransport();
                transport.connect(user, password);
                transport.close();
            }

            EpManager.alert("OK");
            setFinished();
            EpManager.handleState(this, 1);

        }
        catch (MessagingException e)
        {
            catchME(e);
        }

    }
}
