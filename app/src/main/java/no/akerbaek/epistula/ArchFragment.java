package no.akerbaek.epistula;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ArchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ArchFragment extends EpListFragment implements EpFragmentInterface
{

    EpMailFolders sample;

//    ListView simpleList;



    public ArchFragment()
    {
        // Required empty public constructor
        sample = null;
    }

    public static ArchFragment newInstance(String name)
    {
        ArchFragment fragment = new ArchFragment();
        Bundle args = new Bundle();
        args.putString(ARG_NAME, name);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view,savedInstanceState);
        if(sample == null)
            sample = new EpMailFolders(EpManager.getContext(), node);
        //if(sample.isRunning())
        //    ;
        if(sample.isFinised())
            sample.fillArchList();
        else
            EpManager.startRequest(sample, EpManager.getContext());
    }


}