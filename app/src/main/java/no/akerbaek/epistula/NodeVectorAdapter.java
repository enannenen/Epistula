package no.akerbaek.epistula;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class NodeVectorAdapter extends ArrayAdapter<Node>
{

       Node mNode;
       int mResource;


public NodeVectorAdapter(Context context, int resource, int textViewResourceId, Node node)
{
        super(context, resource, textViewResourceId, new Node[node.getSize()]);
        mNode = node;
        mResource = resource;
}

@Override
public int getCount()
{
        return mNode.getSize();
}

@Override
public Node getItem(int p)
    {
        return mNode.getChild(p);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View v = convertView;

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(v==null)
            v = inflater.inflate(mResource, null);

        TextView textView = (TextView) v.findViewById(R.id.textView);
        textView.setText(mNode.getChild(position).getTitle());
        if(mNode.getChild(position).isUnread())
            textView.setTextColor(Color.rgb(200,0,0));

        return v;
    }

}