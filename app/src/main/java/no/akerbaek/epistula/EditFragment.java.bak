package no.akerbaek.epistula;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;


interface Clicker
{
    public void onClick(EpCite c);
}


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditFragment extends EpFragment implements EpFragmentInterface
{
    Spannable content;
    SpannableStringBuilder spanned;

    ScrollView mEnvelope;
    View mEdit;

    public EditFragment()
    {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param args A list of arguments to initiate editing a message

     * @return A new instance of fragment EditFragment.
     */
    public static EditFragment newInstance(Bundle args)
    {
        EditFragment fragment = new EditFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onSaveInstanceState(final Bundle outState)
    {
        super.onSaveInstanceState(outState);
        spanCode(spanned, outState);
    }
/*
    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
    }
*/
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (savedInstanceState != null)
        {
            //probably orientation change
            spanned = (SpannableStringBuilder) spanDecode(savedInstanceState);
        }
        else if (args != null)
        {
            String name = args.getString(ARG_NAME);
            if(node == null)
                node = EpManager.getNode(name);

                content = new SpannableString(args.getString(ARG_TEXT));
                // spanned = new SpannableStringBuilder(args.getString(ARG_TEXT));

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.frag_edit, container, false);

        mEnvelope = (ScrollView) mView.findViewById(R.id.envelope);
        mEnvelope.setOnTouchListener(new EpNodeTouchListener(node));

        return mView;
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view,savedInstanceState);
        TextView name = (TextView) (view.findViewById(R.id.name));
        name.setText(node.getTitle());
        TextView from = (TextView) (view.findViewById(R.id.from));
        String fv = node.getFrom();
        if(fv != null)
            from.setText(fv);

        final EditFragment current=this;
        Button mailButton =  (view.findViewById(R.id.reply));
        mailButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.i("EditFragment","write button clicked");
                EditText cv = view.findViewById(R.id.comment);
                EpCite c = EpCite.getSalute(spanned);
                if(c!=null)
                    showEditor(mEdit,c,mEnvelope);
                else
                    editMail(0,0,R.id.button2);
            }
        });

        Button sendButton =  (view.findViewById(R.id.send));
        sendButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.i("EditFragment","send button clicked");
                EditText cv = view.findViewById(R.id.comment);
                view.setVisibility(View.GONE);
                TextView dv = view.findViewById(R.id.content);
                String mailContent = EpCite.getProducedText(spanned,node.getSubtitle()+ " " + getResources().getString(R.string.wrote) + ":");
                dv.setText(mailContent);
                EpMailSend sample = new EpMailSend(EpManager.getContext(),node);
                sample.setMail(mailContent);
                EpManager.startRequest(sample, EpManager.getContext());
            }
        });

        EpTextView contentView = (EpTextView) (view.findViewById(R.id.content));

        mEdit = view.findViewById(R.id.editpart);
        mEdit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.i("EditFragment", "edit background clicked");
                v.setVisibility(View.GONE);
            }
        });



                Bundle args = getArguments();
        if (args!= null)
        {
            if(spanned == null)
            {
                spanned = new SpannableStringBuilder(args.getString(ARG_TEXT));
                editMail(args.getInt(ARG_START),args.getInt(ARG_END),args.getInt(ARG_TYPE));
            }
            else
                update();

        }

        contentView.setTextIsSelectable(true);
        contentView.setClickable(true);
        contentView.setMovementMethod(LinkMovementMethod.getInstance());


    }

    static void spanCode(SpannableStringBuilder s, Bundle b)
    {
        b.putString(ARG_TEXT, s.toString());
        // TODO: Code-decode all spans into bundle intarrays
    }

    static SpannableStringBuilder spanDecode(Bundle b)
    {
        return new SpannableStringBuilder(b.getString(ARG_TEXT));
    }

    private void update()
    {
        final EpTextView contentView = (EpTextView) (mView.findViewById(R.id.content));
        contentView.setText(spanned);
    }

    private void hideEditor(View edit, EpCite e, View alt)
    {
        EditText commentView = edit.findViewById(R.id.comment);
        edit.setVisibility(View.GONE);
        alt.setVisibility(View.VISIBLE);
        InputMethodManager imm = (InputMethodManager) EpManager.getContext().getSystemService(EpManager.getContext().INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(commentView.getApplicationWindowToken(), 0);
        String ct = commentView.getText().toString();
        e.setCommentText(ct);
        e.clearFlag(0);
        update();
    }

    EpCite mEditCite;

    private void showEditor(View edit, EpCite c, View alt)
    {
        String t;
        EpCite d;
        EditText commentView = edit.findViewById(R.id.comment);
        edit.setVisibility(View.VISIBLE);
        alt.setVisibility(View.GONE);
        edit.setClickable(true);
        edit.setFocusable(true);

        commentView.requestFocus();
        if((d = c.getComment()) != null && (t = d.getCommentText()) != null)
        {
            commentView.setText(t);
        }
        else
            commentView.setText("");
        mEditCite = d!=null?d:c;

        TextView over = edit.findViewById(R.id.over);
        over.setText(EpCite.getProducedText(spanned,node.getSubtitle()+ " " + getResources().getString(R.string.wrote) + ":",null,mEditCite));
        TextView under = edit.findViewById(R.id.under);
        under.setText(EpCite.getProducedText(spanned,node.getSubtitle()+ " " + getResources().getString(R.string.wrote) + ":",mEditCite,null));

        commentView.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                // When focus is lost check that the text field has valid values.

                if (!hasFocus)
                {
                    hideEditor(mEdit,mEditCite,mEnvelope);
                }
            }
        });
    }

    Boolean mClosing = false;

    private class SpanClicker implements Clicker
    {
        private EditText commentView;

        public SpanClicker(EditText cv)
        {
            commentView = cv;
        }

        @Override
        public void onClick(EpCite c)
        {
            Log.i("EditFragment.cite","clicked");
//            if(mClosing)
//                mClosing = false;
//            else
                showEditor(mEdit, c, mEnvelope);
        }

    }

    public void editMail(int start, int stop, int type)
    {
        final EpTextView contentView = (EpTextView) (mView.findViewById(R.id.content));
        final EditText commentView   = (EditText) (mView.findViewById(R.id.comment));

        final EpCite c = new EpCite(spanned, start, stop, new SpanClicker(commentView));
         // Open comment box
        if(type == R.id.button2)
        {
          showEditor(mEdit, c, mEnvelope);
        }
        update();
    }

    // Editfragment needs no paint since no content is loaded
}