package no.akerbaek.epistula;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class EpListFragment extends EpFragment implements EpFragmentInterface
{

    public static EpListFragment newInstance(String name)
    {
        EpListFragment fragment = new EpListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_NAME, name);

        fragment.setArguments(args);
        return fragment;
    }
/*
    @Override
    public void onSaveInstanceState(final Bundle outState)
    {
        super.onSaveInstanceState(outState);
        if(outState != null)
        {
            EpManager.renameNode(node);
            outState.putString(ARG_NAME, node.getName());
        }
    }
*/


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {

// Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.frag_box, container, false);

        ListView listView = mView.findViewById(R.id.boxView);


        return mView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view,savedInstanceState);

        ListView listView = mView.findViewById(R.id.boxView);
        NodeVectorAdapter arrayAdapter = new NodeVectorAdapter( EpManager.getContext(), R.layout.activity_box, R.id.textView, node);
        listView.setAdapter(arrayAdapter);
        listView.setSelection(arrayAdapter.getCount()-1);

        listView.setOnTouchListener(new EpNodeTouchListener(node));
        listView.setOnItemClickListener(new EpOnItemClickListener(node));

    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

    public Boolean paint(Node n)
    {
        ListView listView = mView.findViewById(R.id.boxView);
        NodeVectorAdapter arrayAdapter = (NodeVectorAdapter)listView.getAdapter();
        arrayAdapter.notifyDataSetChanged();
        return true;
    }

}

