package no.akerbaek.epistula;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;

import java.util.BitSet;
import java.util.Calendar;
import java.util.Vector;

public class Node
{
    static final int UNREAD = 0;  // If bit set, this node represents an unread message
    static final int FILTER = 1;  // If bit set, this node represents all the unread messages in a folder
    static final int USER = 2;    // If bit set, this node represents all messages to and from a user


    private final Vector<Node>  mothers;
    private int mIndex;
    private EpFragmentInterface frag;
    private String name;
    private String title;
    private String from;
    private String motherId;
    private Vector<Node> children;
    private int chIndex;                        // Last displayed child
    private Class fragmentType;
    private String folderName;
    private int number;                       // Items position in mailbox
    private Bundle bundle;                    // Arguments to the fragment
    private Vector<EpCite> cites;                     // A list of citations from and comments to a message
    private @StringRes int resId;
    private Calendar calendar;
    private final BitSet bitSet;
    private int partIndex;

    public Node(Node m, String n, Class ft)
    {
        mothers = new Vector<Node>();
        mIndex = 0;
        if(m != null) mothers.add(m);
        frag = null;
        title = folderName = name = n;
        from = null;
        children = new Vector<Node>();
        chIndex = 0;
        fragmentType = ft;
        number = 0;
        if(m != null) m.children.add(this);
        bundle = null;
        resId = 0;
        bitSet = new BitSet(4);
        calendar = null;
        partIndex = -1;
    }
    public Node getMother()
    {
        if(mothers.size()<=mIndex) return null;
        return mothers.get(mIndex);
    }

    public Boolean isMotherOf(Node n)
    {
        return n.mothers.contains(this);
    }

    public void setSingleMother(Node m)
    {
        if(!m.children.contains(this))
            m.children.add(this);
        mothers.removeAllElements();
        mothers.add(m);
        mIndex = 0;
    }

    public void setMotherIndex(Node n)
    {
        int i;
        if((i=mothers.indexOf(n))>=0)
            mIndex=i;
    }

    public Node setMotherIndex(int d)
    {
        int i;
        if((i=mIndex+d)>=0  && i<mothers.size())
            mIndex=i;
        return mothers.get(mIndex);
    }

    public int getMotherIndex()
    {
        return mIndex;
    }

    public int getMothersRight()
    {
        return mothers.size() -  mIndex -1;
    }

    public Node getActiveChild()
    {
        if(children==null) return null;
        if(children.size()<=chIndex) return null;
        return children.get(chIndex);
    }

    public void setChildIndex(Node n)
    {
        int i;
        if((i=children.indexOf(n))>=0)
            chIndex=i;
    }


    public Node getChild(int pos)
    {
        if(children==null) return null;
        if(children.size()<=pos) return null;
        return children.get(pos);
    }
    public Node getNext()
    {
        Node mother = mothers.get(mIndex);
        if(mother == null || mother.children == null) return null;
        if(mother.chIndex+1>=mother.children.size()) return null;
        return mother.children.get(++mother.chIndex);
    }

    public Node getNext(Node mother)
    {
        // TODO: Check if mother is mother
        if(mother == null || mother.children == null) return null;
        if(mother.chIndex+1>=mother.children.size()) return null;
        return mother.children.get(++mother.chIndex);
    }

    public Node getPrev(Node mother)
    {
        // TODO: Check if mother is mother
        if(mother == null || mother.children == null) return null;
        if(mother.chIndex-1<0) return null;
        return mother.children.get(--mother.chIndex);
    }


    public void setChildren(Vector<Node> nl){children = nl;}
    public Vector<Node> getChildren(){return children;}

    public void addChild(Node n)
    {
        if(!children.contains(n))
            children.add(n);
        if(!n.mothers.contains(this))
            n.mothers.add(this);
    }

    public void addChildren(Vector<Node> nl)
    {
        children.addAll(nl);
    }
    public void addMother(Node m)
    {
        if(!m.children.contains(this))
            m.children.add(this);
        if(!mothers.contains(m))
            mothers.add(m);
    }

    public void addBabysitter(Node m)
    {
        // Take care of the children but do not identify as mother
        if(!m.children.contains(this))
            m.children.add(this);
    }

    // Move to another mother
    public void moveTo(Node m)
    {
        if(!m.children.contains(this))
            m.children.add(this);
        /*
        // Moving the mother (folders) one item up
        for (Node p: mothers)
        {
            if(p != m)
                m.mothers.add(p);
        }
         */
        mothers.removeAllElements();
        mothers.add(m);
    }

    // Move to another mother
    public void transferTo(Node s, Node d)
    {
        if(d != null && d.children != null && !d.children.contains(this))
            d.children.add(this);
        if(s != null && s.children != null)
            s.children.remove(this);

        mothers.remove(s);
        mothers.add(d);
    }

    public void repairIndexes(Node m)
    {
        Node n;
        for(int i=0;i<children.size();i++)
        {
            n = children.get(i);
            if(n.number >= m.number)
                n.number = 0;  // Do not use folder/number to retrieve message from server
        }
    }

    public EpFragmentInterface getFragment() throws IllegalAccessException, Fragment.InstantiationException
    {
        if(frag == null)
        {
            EpFragment f=null;
            EpSettingsFragment g=null;
            if(fragmentType == ArchFragment.class)
                    f = (EpFragment) ArchFragment.newInstance(name);
            else if(fragmentType == BoxFragment.class)
                    f = (EpFragment) BoxFragment.newInstance(name);
            else if(fragmentType == MailFragment.class)
                f = (EpFragment) MailFragment.newInstance(name);
            else if(fragmentType == EditFragment.class)
                f = (EpFragment) EditFragment.newInstance(bundle);
            else if(fragmentType == EpFragment.class)
                f = (EpFragment) EpFragment.newInstance(name);
            else if(fragmentType == EpListFragment.class)
                f = (EpFragment) EpListFragment.newInstance(name);
            else if(fragmentType == EpSettingsFragment.class)
                g = (EpSettingsFragment) new EpSettingsFragment();
            else if(fragmentType == null)
                return null;
            else
                    throw new IllegalStateException("Unexpected value: " + fragmentType);
            if(f!=null)
            {
                f.setNode(this);
                frag = f;
            }
            else if(g!=null) frag = g;
        }

        return frag;
    }

    public String getName()
    {
        return name;
    }
    public void setName(String s)
    {
        name = s;
    }

    public View getView()
    {
        if(frag == null) return null;
        return ((Fragment)frag).getView();
    }

    public String getFolderName()
    {
        return folderName;
    }
    public void setFolderName(String s)
    {
        folderName = s;
    }
    public String getTitle()
    {
        return title;
    }
    public void setTitle(String s)
    {
        title = s;
    }
    public String getSubtitle()
    {
        String s;
        if(from != null)
            s = from.split("[ @]",2)[0];
        else
            s = title;
        if(s.length() > 12) s = s.substring(0,12);
        if(getType() == BoxFragment.class) return "[" + s + "]";
        if(getType() == MailFragment.class) return "(" + s + ")";
        if(getType() == ArchFragment.class) return "{" + s + "}";
        return s;
    }
    public String getFrom()
    {
        return from;
    }
    public void setFrom(String s)
    {
        from = s;
    }

    public void setType(Class c)
    {
        if(fragmentType != c)
        {
            fragmentType = c;
            frag = null;
        }
    }

    public int getNumber()
    {
        return number;
    }
    public void setNumber(int n)
    {
        number = n;
    }
    public int getEnd()
    {
        return partIndex;
    }
    public void setEnd(int n)
    {
        partIndex = n;
    }
    public int getSize()
    {
        return children.size();
    }

    public int getPosition()
    {
        return chIndex;
    }

    public Class getType()
    {
        return fragmentType;
    }

    public void setMotherId(String n)
    {
        motherId = n;
    }
    public String getMotherId()
    {
        return motherId;
    }

    public void setResourceId(@StringRes int id)
    {
        resId = id;
    }
    public int getResourceId()
    {
        return resId;
    }

    public void setCalendar(Calendar c)
    {
        calendar = c;
    }
    public Calendar getCalendar()
    {
        return calendar;
    }


    public void removeChild(Node n)
    {
        children.remove(n);
        n.mothers.remove(this);
    }



    public Boolean isChildrenUnread(int pos, int step)
    {
        for(int i=pos+step;i<children.size()&&i>=0;i+=step)
            if(children.get(i).isUnread()) return true;
        return false;
    }

    public Boolean isUnread()
    {
        if (BuildConfig.DEBUG && Thread.currentThread().getStackTrace().length > 100)
        {
            throw new AssertionError("Assertion failed, stacksize " + Thread.currentThread().getStackTrace().length);
        }

        if (bitSet.get(UNREAD))
            return true;
        return isChildrenUnread(-1, 1); // Check all children of child
    }

    void setUnread(Boolean r)
    {
        bitSet.set(UNREAD,r);
    }

    public Boolean isFilter()
    {
        return bitSet.get(FILTER);
    }
    void setFilter(Boolean r)
    {
        bitSet.set(FILTER,r);
    }

    public Boolean isUser()
    {
        return bitSet.get(USER);
    }
    void setUser(Boolean u)
    {
        bitSet.set(USER,u);
    }

    public Boolean isPermanent()
    {
        return (number != 0);
    }

    void setArguments(Bundle b)
    {
        bundle = b;
    }

    public Vector<EpCite> getCites()
    {
        return cites;
    }
    void setCites(Vector<EpCite> c)
    {
        cites = c;
    }

}

