package no.akerbaek.epistula;

import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.search.ComparisonTerm;
import javax.mail.search.FlagTerm;
import javax.mail.search.FromTerm;
import javax.mail.search.OrTerm;
import javax.mail.search.ReceivedDateTerm;
import javax.mail.search.RecipientTerm;
import javax.mail.search.SearchTerm;

class EpMailList extends EpMailbase implements Mailrequest
{

    public EpMailList(MainActivity m, Node n)
    {
        super(m, n);
    }

    Date prepareDate(Calendar cal)
    {
        Calendar now = Calendar.getInstance();
        int doy = now.get(Calendar.DAY_OF_YEAR);

        if(cal.get(Calendar.YEAR) == now.get(Calendar.YEAR)
                && cal.get(Calendar.DAY_OF_YEAR) == doy)
        {
            now.set(Calendar.DAY_OF_YEAR, doy - 1);
            return now.getTime();
        }
        else
            return cal.getTime();
     }

    public void mailListIn(Node node) throws MessagingException, UnsupportedEncodingException
    {
        final int dist = 20;
        Message[] messages;
        Calendar rDate = node.getCalendar();
        Vector<Node> present = null;
        if(rDate == null)
        {
            int rEnd = node.getEnd();
            if(rEnd < 0) rEnd = inbox.getMessageCount();
            int s = Math.max(1,rEnd - dist);
            messages = inbox.getMessages(s, rEnd);
            node.setEnd(s);

            present = node.getChildren();
            node.setChildren(new Vector<Node>());

            // TODO: Trenger argument for å velge innhenting av eldre
        }
        else
        {
            SearchTerm newerThan = new ReceivedDateTerm(ComparisonTerm.GT, prepareDate(rDate));
            messages = inbox.search(newerThan);
        }
        System.out.println("Number of mails = " + messages.length);
        for (Message message : messages)
        {
            inMail(node,message);
        }

        if(present != null)
            node.addChildren(present);

    }




    public void mailRecentIn(Node node) throws MessagingException, UnsupportedEncodingException
    {
        if(!inbox.isOpen()) inbox.open(Folder.READ_ONLY);
        Message[] messages;
        messages = inbox.search(
                new FlagTerm(
                        new Flags(Flags.Flag.SEEN), false));
        Log.i("mailRecentIn","Number of mails = " + messages.length);
        // Node mother = EpManager.getNode(node.getFolderName());  // Find the actual folder node of the proxy
        for (Message message : messages)
        {
            // Mailbase.inMail(mother, node, message);
            inMail(node, node, message);
        }

    }

    public void mailUserIn(Node node) throws MessagingException, UnsupportedEncodingException
    {
        String a = EpManager.getEmailAddress(node.getName());
        if(a == null) return;
        InternetAddress ib = new InternetAddress(a);  // Repack with only address part



        Message[] messages;
        messages = inbox.search(new OrTerm(new FromTerm(ib), new RecipientTerm(Message.RecipientType.TO,ib)));
//TODO:If node calendar not null andterm ReceivedDateTerm
        Log.i("mailUserIn","Number of mails = " + messages.length);
        for (Message message : messages)
        {
            // Mailbase.inMail(mother, node, message);
            inMail(node, message);
        }

    }

    public void doIt()
    {
        super.doIt();
        try
        {

            store = session.getStore("imaps");
            if(!store.isConnected()) store.connect();
            String fn = (node.getFolderName() != null) ? node.getFolderName() : "INBOX";
            System.out.println("Folder name = " + fn);

            //Vector<Node> ochl = node.getChildren();
            //Vector<Node> nchl = new Vector<Node>();
            //node.setChildren(nchl);

            String [] folderList;
            if(fn.equals("all"))
            {
                Node cats = EpManager.getFolders();
                folderList = new String[cats.getSize()];
                int j=0;
                for (int i = 0; i < cats.getSize(); i++)
                {
                    Node f = cats.getChild(i);
                    String fnp = f.getFolderName();
                    if(fnp.contentEquals("Trash")) continue;  // Skip trash folder
                    folderList[j++] = fnp;
                }
            }
            else
            {
                folderList = new String[1];
                folderList[0] = fn;
            }

            for (int i = 0; i < folderList.length; i++)
            {
                String fnq = folderList[i];
                if(fnq==null)continue;

                inbox = store.getFolder(fnq);
                if(inbox.isOpen()) continue;
                Log.i("EpMailList", "Open " + fnq + ", i " + i);
                inbox.open(Folder.READ_ONLY);

                if (node.isFilter())
                    mailRecentIn(node);
                else if(node.isUser())
                    mailUserIn(node);
                else
                    mailListIn(node);

                Log.i("EpMailList", "Read " + fnq + ", up to " + node.getSize() + " children ");

                if(inbox.isOpen())
                {
                    Log.i("EpMailList", "Closing " + fnq );
                    inbox.close(true);
                }
                else
                    Log.i("EpMailList", "Not closing " + fnq );

             }
            node.setCalendar(Calendar.getInstance());
            store.close();
            setFinished();
            EpManager.handleState(this, 1);
            // Unhinge old children that is not new
            //for(Node c: ochl)
            //{
            //    if(!nchl.contains(c))
            //        node.removeChild(c);
            //}
        }
        catch (MessagingException e) { catchME(e); }
        catch (UnsupportedEncodingException e) { e.printStackTrace(); }
    }

    public void postIt(int what) throws InstantiationException, IllegalAccessException
    {


        super.postIt(what);
        EpManager.reGrowTree();
        EpFragment fragment = (EpFragment)node.getFragment();
        fragment.paint(node);
    }


}

