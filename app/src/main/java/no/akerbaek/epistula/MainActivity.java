package no.akerbaek.epistula;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;


public class MainActivity extends AppCompatActivity
{

    SharedPreferences.OnSharedPreferenceChangeListener mListener;

    @Override
    public void onSaveInstanceState(@NonNull Bundle savedInstanceState)
    {
        super.onSaveInstanceState(savedInstanceState);
        // Save UI state changes to the savedInstanceState.
        // Just used as a dummy,the actual state is in Epmanager
        savedInstanceState.putBoolean("MyBoolean", true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        EpManager.setContext(this);
        setContentView(R.layout.activity_main);

        if(savedInstanceState != null)
            EpManager.navigateTo(EpManager.getCurrentNode());
        else
            EpManager.navigateTo(EpManager.initStructure());


        /*
        on start: init structure, if first time, go to settings else go to unread mails
        on reorientation: keep structure keep current node
        on settings/language change: init structure, keep current node

        TODO: Separate navigate from initstructure
         */

        Button nextButton = (Button) findViewById(R.id.next);
        nextButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.i("MainActivity","post button clicked");
                // Node cNode = EpManager.getCurrentNode();
                Node next = EpManager.getNextNode();
                if (next == null)
                    return;
                next.setMotherIndex(EpManager.getCurrentMother());
                EpManager.navigateTo(next);
            }
        });

        Button prevButton = (Button) findViewById(R.id.prev);
        prevButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                System.out.println("prev button clicked");
                // Node cNode = EpManager.getCurrentNode();
                Node p = EpManager.getPrevNode();
                if (p == null)
                    return;
                p.setMotherIndex(EpManager.getCurrentMother());
                EpManager.navigateTo(p);
            }
        });

        Button downButton = (Button) findViewById(R.id.down);
        downButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                System.out.println("down button clicked");
                Node cNode = EpManager.getCurrentNode();
                Node p = cNode.getActiveChild();
                if (p == null)
                    return;
                p.setMotherIndex(cNode);

                EpManager.navigateTo(cNode,p);
            }
        });

        Button upButton = (Button) findViewById(R.id.up);
        upButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                System.out.println("up button clicked");
                Node cNode = EpManager.getCurrentNode();

                Node p = EpManager.getCurrentMother();

                if (p == null)
                    return;
                p.setChildIndex(cNode);  // Remember where we came from

                EpManager.navigateTo(p.getMother(),p);
            }
        });

        Button maLeftButton = (Button) findViewById(R.id.maleft);
        maLeftButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                System.out.println("left top button clicked");
                Node cNode = EpManager.getCurrentNode();
                EpManager.setCurrentMother(cNode.setMotherIndex(-1));

                EpManager.fixButtons();
            }
        });

        Button maRightButton = (Button) findViewById(R.id.maright);
        maRightButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                System.out.println("right top button clicked");
                Node cNode = EpManager.getCurrentNode();
                EpManager.setCurrentMother(cNode.setMotherIndex(+1));


                EpManager.fixButtons();
            }
        });

        Button reloadButton = (Button) findViewById(R.id.reload);
        reloadButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.i("MainActivity","reload button clicked");
                // Node cNode = EpManager.getCurrentNode();
                EpManager.reloadCurrent();
            }
        });

        Button houseButton = (Button) findViewById(R.id.home);
        houseButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Log.i("MainActivity","house button clicked");
                Node p = EpManager.getRoot();
                EpManager.navigateTo(null,p);
            }
        });

        class EpSOnSharedPreferenceChangeListener implements SharedPreferences.OnSharedPreferenceChangeListener
        {
            public void onSharedPreferenceChanged(SharedPreferences prefs, String key)
            {
                if (key.equals("app_lang"))
                {
                    String languagePref_ID = prefs.getString(key, "");
                    EpManager.swapLanguage(languagePref_ID);
                }
            }
        }


        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(EpManager.getContext());
        mListener = new EpSOnSharedPreferenceChangeListener();
        sharedPref.registerOnSharedPreferenceChangeListener(mListener);


    }







}
// Missing features

// TODO: Move complete thread to folder


// TODO: Read and save attachments

// TODO: Indent citation in answers
// TODO: Fix touch speed in ontouchlistener
// TODO: Improve edit layout

