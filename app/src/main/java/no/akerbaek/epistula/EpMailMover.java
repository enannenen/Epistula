package no.akerbaek.epistula;

import android.util.Log;

import com.sun.mail.imap.IMAPFolder;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Store;
import javax.mail.search.HeaderTerm;

class EpMailMover extends EpMailbase implements Mailrequest
{

    String mDestination;

    public EpMailMover(MainActivity m, Node n, String archive)
    {
        super(m, n);
        mDestination = archive;
    }

    String mailContent;

    public void setMail(String m)
    {
        mailContent = m;
    }

    public String getMail()
    {
        return mailContent;
    }


    @Override
    public void doIt()
    {
        super.doIt(); // Establish session on our own

        try
        {
            // Copy message to "Sent Items" folder as read
            Store store = session.getStore("imaps");
            store.connect();

            String fn = (node.getFolderName() != null) ? node.getFolderName() : "INBOX";
            IMAPFolder source = (IMAPFolder) store.getFolder(fn);
            if(!source.isOpen()) source.open(Folder.READ_WRITE);
            Message message;
            int n = node.getNumber();
            if(n > 0 && n <= source.getMessageCount())
                message = source.getMessage(node.getNumber());
            else
                message = source.search(new HeaderTerm("Message-ID",node.getName()))[0];

            Folder dest = store.getFolder(mDestination);

            if(!dest.exists())
                dest.create(Folder.HOLDS_MESSAGES);
            message.setFlag(Flags.Flag.SEEN, true);
            source.moveMessages (new Message[] {message}, dest);
            store.close();

            Log.i("Mailmover.doit","message copied to folder " + mDestination);

        }
        catch (MessagingException e)
        {
            catchME(e);
            EpManager.handleState(this,6);
            return;
        }

        EpManager.handleState(this,7);
    }


    public void postIt(int what) throws InstantiationException, IllegalAccessException
    {
        super.postIt(what);
        Node m = EpManager.getCurrentMother();
        Node next = EpManager.getNextNode();
        if(next==null)
            next = EpManager.getPrevNode();
        if(next==null)
            next = m;

        // TODO: Check if we still are at position of removed node
        if(what==6)
            EpManager.navigateTo(EpManager.getSettings());  // Error situation, fix settings
        else
            EpManager.navigateTo(next);

        Node from = EpManager.getNode(node.getFolderName());  // Remove from local folder proxy
        Node to = EpManager.getNode(mDestination);
        node.transferTo(from,to);
        from.repairIndexes(node);

        if(m!=from)
        {
            m.removeChild(node);
            m.repairIndexes(node);
        }

        EpManager.fixButtons();

    }

}

