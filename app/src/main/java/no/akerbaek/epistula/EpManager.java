package no.akerbaek.epistula;


import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.preference.PreferenceManager;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;

class EpManager
{
    // static final int FOLDERLIST=1;
    // static final int ALLFOLDERS=2;

    static EpManager sInstance;
    // A queue of Runnables
    private final BlockingQueue<Runnable> decodeWorkQueue;

    private final ThreadPoolExecutor decodeThreadPool;

    /*
     * Gets the number of available cores
     * (not always the same as the maximum number of cores)
     */
    private static final int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();
    // Sets the amount of time an idle thread waits before terminating
    private static final int KEEP_ALIVE_TIME = 1;
    // Sets the Time Unit to seconds
    private static final TimeUnit KEEP_ALIVE_TIME_UNIT;
    // Creates a thread pool manager


    // An object that manages Messages in a Thread
    private static Handler handler;
    private static MainActivity main;

    private static Node mNode;
    private static Node mMother;
    private static Node mRadix;
    private static Node mSettings;
    private static Node mFolders;
    private static Node mContacts;

    //TODO: Use concurrent version
    private static HashMap<String,Node> repository;  // Created nodes
    private static HashMap<String,Node> motherless;  // Nodes without permanent place in node three

    static
    {
        // Sets the Time Unit to seconds
        KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;

        // Creates a single static instance of EpManager
        sInstance = new EpManager();
    }

    private static String mDomain  = null;
    private static String mAddress = null;
    // private static MainActivity appContext;

    private static int mGuard = 0;

    private EpManager()
    {
        // Instantiates the queue of Runnables as a LinkedBlockingQueue
        decodeWorkQueue = new LinkedBlockingQueue<Runnable>();

        decodeThreadPool = new ThreadPoolExecutor(
                NUMBER_OF_CORES,       // Initial pool size
                NUMBER_OF_CORES,       // Max pool size
                KEEP_ALIVE_TIME,
                KEEP_ALIVE_TIME_UNIT,
                decodeWorkQueue);


        // Defines a Handler object that's attached to the UI thread
        handler = new Handler(Looper.getMainLooper())
        {
            /*
             * handleMessage() defines the operations to perform when
             * the Handler receives a new Message to process.
             */
            @Override
            public void handleMessage(android.os.Message inputMessage)
            {

                Mailrequest sample = (Mailrequest) inputMessage.obj;
                try
                {
                    sample.postIt(inputMessage.what);
                } catch (java.lang.InstantiationException e)
                {
                    e.printStackTrace();
                } catch (IllegalAccessException e)
                {
                    e.printStackTrace();
                }
                stopSpin();

            }
        };

        repository = new HashMap<String, Node>();
        motherless = new HashMap<String, Node>();

        mNode = null;
        mMother = null;
    }

    private static void stopSpin()
    {
        if(mGuard > 1)  mGuard--;
        else if(mGuard == 1)
        {
            mGuard--;
            ProgressBar spinner;
            spinner = (ProgressBar)main.findViewById(R.id.progressBar1);
            spinner.setVisibility(View.GONE);
        }
    }

    private static void startSpin()
    {
        mGuard++;
        ProgressBar spinner;
        spinner = (ProgressBar)main.findViewById(R.id.progressBar1);
        spinner.setVisibility(View.VISIBLE);
    }

    public static void startRequest(
            Mailrequest task,
            MainActivity mainActivity)
    {
        main = mainActivity;

        task.init();
        // Adds a download task to the thread pool for execution
        sInstance.decodeThreadPool.execute(task.getRunnable());

        startSpin();

    }

    public static void handleState(Mailrequest request, int state)
    {
        android.os.Message completeMessage =
                handler.obtainMessage(state, request);
        completeMessage.sendToTarget();
    }


    public static void setContext(MainActivity c)
    {
        main = c;
    }

    public static MainActivity getContext()
    {
        return main;
    }

    public static void setCurrentNode(Node n)
    {
        mNode = n;
    }

    public static Node getCurrentNode()
    {
        return mNode;
    }

    public static void setCurrentMother(Node n)
    {
        mMother = n;
    }

    public static Node getCurrentMother()
    {
        return mMother;
    }

    public static Node getNextNode()
    {
        return mNode.getNext(mMother);
    }

    public static Node getPrevNode()
    {
        return mNode.getPrev(mMother);
    }

    private static void fixMother(Node n)
    {
        if(n.getType() != MailFragment.class) return;
        Node m = n.getMother();
        if(m.getType() != BoxFragment.class) return;

        // Boolean perm = false;
        String mid  = n.getMotherId();
        Node mn = EpManager.getNode(mid);
        if(mn == null) return;
        if(mn == m) return;
        /*
        String mpar = getParentId(message);
        Node mn=null;
        if(!mpar.isEmpty())
            mn = EpManager.getNode(mpar);
        if(mn==null)
            mn = node;
         else
            perm = true;

         */

        // Moving to folder
//        if(mn.getType()==BoxFragment.class)
        n.addMother(mn);
            /*
        // Moving to mail
        else if(mn.getType()==MailFragment.class)
            if(m.getRecent())  // Do not delete from unread
                n.addMother(mn);
            else
                n.moveTo(mn);
*/
    }

    public static void  navigateTo(Node n)
    {
//        fixMother(n);
        Fragment f;
        try
        {
            f = (Fragment) n.getFragment();
        } catch (Fragment.InstantiationException e)
        {
            e.printStackTrace();
            return;
        } catch (IllegalAccessException e)
        {
            e.printStackTrace();
            return;
        }
/*
        // TODO: Filter out repeated navigation to the same node
        if(mNode == n)
        {
            Log.i("manager.navigateto","Navigate to self skipped");
            return;
        }
 */
        FragmentTransaction ft = main.getSupportFragmentManager().beginTransaction();
        if(mNode != null) ft.addToBackStack(null);
        ft.replace(R.id.frame_container, f).commit();
//        if(f instanceof EpSettingsFragment)
//            setCurrentNode(n); // Settings is not EpFragment.
    }

    public static void  navigateTo(Node mother, Node n)
    {
        setCurrentMother(mother);
        navigateTo(n);
    }

    public static void reInitFragment(Node n, Class type)
    {
        Fragment fragment;
        try
        {
            fragment = (Fragment)n.getFragment();
        } catch (Fragment.InstantiationException e)
        {
            e.printStackTrace();
            return;
        } catch (IllegalAccessException e)
        {
            e.printStackTrace();
            return;
        }
        FragmentManager fm = main.getSupportFragmentManager();
        fm.beginTransaction().remove(fragment).commit();
        fm.popBackStack();
        n.setType(type);
    }

    public static void reloadCurrent()
    {
        try
        {
            EpFragmentInterface f = mNode.getFragment();
            f.reload();
        } catch (Fragment.InstantiationException e)
        {
            e.printStackTrace();
        } catch (IllegalAccessException e)
        {
            e.printStackTrace();
        }
    }


    public static void fixButtons()
    {

        if(main != null)
        {
            EpNodeButton anteButton = main.findViewById(R.id.prev);
            String bS;
            int pos = (mMother==null)? 0 : mMother.getPosition();
            anteButton.setRecent(mMother != null && mMother.isChildrenUnread(pos,-1));
            if(pos >0)
            {
                bS = main.getResources().getString(R.string.left) + pos;
                anteButton.setEnabled(true);
            }
            else
            {
                bS = main.getResources().getString(R.string.left);
                anteButton.setEnabled(false);
            }
            anteButton.setText(bS);
            //anteButton.setTextColor(Color.argb(a,r,g,0));

            EpNodeButton postButton = main.findViewById(R.id.next);
            int rem;
            if(mMother!=null) rem = mMother.getSize() -pos - 1;
            else rem = 0;
            postButton.setRecent(mMother != null && mMother.isChildrenUnread(pos,1));
            if(rem >0)
            {
                bS = "" + rem + main.getResources().getString(R.string.right);
                postButton.setEnabled(true);
            }
            else
            {
                bS = main.getResources().getString(R.string.right);
                postButton.setEnabled(false);
            }
            postButton.setText(bS);

            Button maLeftButton = main.findViewById(R.id.maleft);
            if(mNode.getMotherIndex() > 0)
            {
                maLeftButton.setEnabled(true);
            }
            else
            {
                maLeftButton.setEnabled(false);
            }
            Button maRightButton = main.findViewById(R.id.maright);
            if(mNode.getMothersRight() > 0)
            {
                maRightButton.setEnabled(true);
            }
            else
            {
                maRightButton.setEnabled(false);
            }
            EpNodeButton downButton = main.findViewById(R.id.down);
            int ch;
            downButton.setRecent(mNode.isChildrenUnread(-1,1));
            ch = mNode.getSize();
            if(ch >0)
            {
                bS = "(" + ch + ")";
                downButton.setEnabled(true);
            }
            else
            {
                bS = main.getResources().getString(R.string.down);
                downButton.setEnabled(false);
            }
            downButton.setText(bS);
            Button upButton = main.findViewById(R.id.up);
            if(mMother!=null)
            {
                bS = mMother.getSubtitle();
                upButton.setEnabled(true);
            }
            else
            {
                bS = main.getResources().getString(R.string.up);
                upButton.setEnabled(false);
            }
            upButton.setText(bS);
        }
    }

    public static Node createNode(Node m, String n, Class ft, Boolean permanent)
    {
        Node ret;

        if ((ret = repository.get(n)) != null)
            repository.remove(n);
        else if ((ret = motherless.get(n)) != null)
            motherless.remove(n);

        if(ret==null)
            ret = new Node(m, n, ft);
        else
        {
            if(m==null)
                ;
            else if(m.getType()==BoxFragment.class)
                ret.addMother(m);
            else if(m.getType()==MailFragment.class)
                ret.addMother(m);  // ret.moveTo(m);
            else if(m.getType()==ArchFragment.class)
                ret.setSingleMother(m);
            ret.setType(ft);
        }
        if(permanent)
            repository.put(n, ret);
        else
            motherless.put(n,ret);
        return ret;
    }

    public static Node createNode(Node m, @StringRes int id, Class ft, Boolean permanent)
    {
        String name = main.getResources().getString(id);
        Node ret = createNode(m,name,ft,permanent);
        ret.setResourceId(id);
        return ret;
    }

    public static void renameNode(Node n)
    {
        int id = n.getResourceId();
        if(id==0)
            return;
//        String on = n.getName();
        String nn = main.getResources().getString(n.getResourceId());
//        n.setName(nn);
//
            n.setTitle(nn);
//        repository.remove(on);
//        repository.put(nn, n);

    }

    public static Node getNode(String n)
    {
        Node ret = repository.get(n);
        if(ret==null)
            ret = motherless.get(n);
        return ret;
    }

    public static void reGrowTree()
    {
        // Iterate through motherless nodes
        // Check if mother has appeared
        // Move child to mother

        List<String> objectsToRemove = new ArrayList<>();
        for (HashMap.Entry<String, Node> entry : motherless.entrySet())
        {
            String key = entry.getKey();
            Node node = entry.getValue();
            String mid = node.getMotherId();
            if(mid == null || mid.isEmpty()) continue;
            Node mn = getNode(mid);
            if(mn != null)
            {
                if(mn.isMotherOf(node)) continue;
                // node.addMother(mn);
                node.moveTo(mn);
                repository.put(key,node);
                objectsToRemove.add(key);
            }
        }
        for (String k : objectsToRemove)
        {
            motherless.remove(k);
        }

    }

    public static Node getRoot()
    {
        return mRadix;
    }
    public static Node getSettings()
    {
        return mSettings;
    }
    public static Node getContacts()
    {
        return mContacts;
    }
    public static Node getFolders()  { return mFolders;  }

    public static Node initStructure()
    {
        if(mRadix == null)
        {
            Node m = EpManager.createNode(null, R.string.radix, EpListFragment.class, true);
            EpManager.navigateTo(m);
            mMother = mRadix = m;
            mSettings = EpManager.createNode(m, R.string.settings, EpSettingsFragment.class, true);
            mFolders = EpManager.createNode(m, R.string.catalogi, ArchFragment.class, true);
            EpManager.navigateTo(mFolders);
            mContacts = EpManager.createNode(m, R.string.clientes, EpListFragment.class, true);
            // Node me = EpManager.createNode(contacts,"tord@akerbaek.no",BoxFragment.class, true);
            // me.setFolderName("all");
            // me.setUser(true);
            // Node inbox =
            EpManager.createNode(mFolders, "INBOX", BoxFragment.class, true);
            // EpManager.navigateTo(inbox);
        }
        Node novae = EpManager.createNode(mRadix, R.string.novae, BoxFragment.class, true);
        novae.setFolderName("INBOX");
        novae.setFilter(true);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(main); // main.getPreferences(Context.MODE_PRIVATE);

        String languagePref_ID = prefs.getString("app_lang", "");
        EpManager.swapLanguage(languagePref_ID);
        String userName = prefs.getString("imap_user",null);// check if user name set
        if(userName == null) return mSettings;
        else return novae;
    }

    public static void translateStructure()
    {
        renameNode(mRadix);
        for(int i=0;i<mRadix.getSize();i++)
            renameNode(mRadix.getChild(i));
    }

    private static void setLocale(Locale locale)
    {
        Locale.setDefault(locale);
        Resources res = main.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        // Configuration conf = new Configuration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
        {
            conf.setLocale(locale);
        }
        else
        {
            conf.locale = locale;
        }
        res.updateConfiguration(conf, dm);
        EpManager.translateStructure();
        EpManager.getContext().recreate();
    }

    public static void swapLanguage(String shortCode)
    {
        switch (shortCode)
        {
            case "la":
                Locale localeLA = new Locale("la");
                setLocale(localeLA);
                break;
            case "no":
                Locale localeNO = new Locale("no","NO");
                setLocale(localeNO);
                break;
            case "en":
                Locale localeEN = new Locale("en","GB");
                setLocale(localeEN);
                break;

        }

    }

    private static String createDomain()
    {
        if(mAddress == null)
        {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(main); // main.getPreferences(Context.MODE_PRIVATE);
            mAddress = prefs.getString("your_address", "name@host.no");
        }

        mDomain = mAddress .substring(mAddress .indexOf("@") + 1);
        return mDomain;
    }

    public static String createId()
    {

        if(mDomain == null) mDomain = createDomain();
        Date d = new Date();
        return "<" + UUID.randomUUID().toString() + "." + Math.round(d.getTime()*Math.PI)%987653  + "@" + mDomain + ">";
    }

    public static String getEmailAddress(String source)
    {
        try
        {
            InternetAddress ia = new InternetAddress(source);
            String a = ia.getAddress();
            if(a.contains("@"))
                return a;
            else
                return null;
        } catch (MessagingException e)
        {
          e.printStackTrace();
        }
        return null;
     }

     public static void alert(String s)
     {
         View v = EpManager.getCurrentNode().getView();
         // TODO: Only make snackbar if v has suitable parent
         if(v == null) v = getContext().findViewById(android.R.id.content);
         Snackbar snackBar = Snackbar.make(v, s, Snackbar.LENGTH_LONG);
         snackBar.show();
     }
}
