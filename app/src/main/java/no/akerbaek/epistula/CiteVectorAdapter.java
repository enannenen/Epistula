package no.akerbaek.epistula;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Vector;

public class CiteVectorAdapter extends ArrayAdapter<EpCite>
{

    Vector<EpCite> mCites;
    int mResource;
    String mContent;

    public CiteVectorAdapter(Context context, int resource, int textViewResourceId, Vector<EpCite> cites, String content)
    {
        super(context, resource, textViewResourceId, new EpCite[cites.size() + 10]);
        mCites = cites;
        mResource = resource;
        mContent = content;
    }

    @Override
    public int getCount()
    {
        return mCites.size();
    }

    @Override
    public EpCite getItem(int p)
    {
        return mCites.get(p);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {

        View v = convertView;
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(mResource, null);
        TextView citeView = (TextView) v.findViewById(R.id.cite);
        EditText commentView = (EditText) v.findViewById(R.id.comment);

        citeView.setText(mCites.get(position).getCiteText());
        //commentView.setText(mCites.get(position).comment);
        String deb = mCites.get(position).getCiteText();

        return v;

    }

}