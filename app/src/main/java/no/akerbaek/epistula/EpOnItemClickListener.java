package no.akerbaek.epistula;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

public class EpOnItemClickListener implements AdapterView.OnItemClickListener
{
    private final Node node;

    EpOnItemClickListener(Node n)
    {
       node = n;
    }

    public void onItemClick(AdapterView<?> l, View v, int position, long id)
    {
        TextView tv = v.findViewById(R.id.textView);
        NodeVectorAdapter a = (NodeVectorAdapter) l.getAdapter();
        Node spot = a.getItem(position);
        String fn = (String) ((TextView)tv).getText();
        // Node spot = node.getChild(position);
        spot.setMotherIndex(node);  // Remember where we came from TODO:evaluate this
        if(spot.getMother() != null)
        {
            Log.i("ReadMailList", "You clicked Item: " + position + " with text:" + fn + " and name " + spot.getName());
            spot.getMother().setChildIndex(spot);  // Remenber where we started
            EpManager.navigateTo(node, spot);
        }
        else
            Log.i("ReadMailList", "You clicked Item: " + position + " with text:" + fn + " and no content");

    }

}
