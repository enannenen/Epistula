package no.akerbaek.epistula;

import android.util.Log;
import android.view.View;
import android.widget.ListView;

import javax.mail.MessagingException;

public class EpMailFolders extends EpMailbase implements Mailrequest
{
    // String[] mailList;

    public EpMailFolders(MainActivity m, Node n)
    {
        super(m, n);
    }


    public void fillArchList()
    {
        ListView simpleList = (ListView)(node.getView().findViewById(R.id.boxView));
        NodeVectorAdapter arrayAdapter = new NodeVectorAdapter( main, R.layout.activity_box, R.id.textView, node);
        simpleList.setAdapter(arrayAdapter);
    }


    public void doIt()
    {
        super.doIt();

        try
        {
            store = session.getStore("imaps");
            store.connect();
            javax.mail.Folder[] folders = store.getDefaultFolder().list("*");
            for (javax.mail.Folder folder : folders)
            {
                if ((folder.getType() & javax.mail.Folder.HOLDS_MESSAGES) != 0)
                {
                    EpManager.createNode(node,folder.getFullName(),BoxFragment.class, true);
                    Log.i("ReadFolderNames.doIt",folder.getFullName() + " : " + folder.getMessageCount());
                }
            }
            store.close();
            setFinished();

            EpManager.handleState(this, 2);
        }
        catch (MessagingException e) { catchME(e); }
    }

    public void postIt(int what) throws InstantiationException, IllegalAccessException
    {
        super.postIt(what);

        View v = node.getView();
        if(v != null)
        {
            fillArchList();
        }

    }

}
